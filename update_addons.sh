#!/bin/bash
PROJECT_DIR=$(dirname "$0")

if [ -z "${OF_ROOT}" ]; then
    echo "OF_ROOT enviroment variable is undefined."

    read -p "Is your app in apps/myApps folder? " -n 1 -r
    echo    # (optional) move to a new line
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        OF_ROOT=$PROJECT_DIR/../../..
    else
        echo "Rerun this script setting OF_ROOT enviroment variable: $ export OF_ROOT=<path/to/of>\n "
        exit
    fi
fi

cd $OF_ROOT/addons/ofxMidi; git pull
cd $OF_ROOT/addons/ofxSimpleTimer; git pull
cd $OF_ROOT/addons/ofxJSON; git pull
cd $OF_ROOT/addons/ofxConvexHull; git pull
cd $OF_ROOT/addons/ofxTweener; git pull
cd $OF_ROOT/addons/ofxImGui; git pull
cd $OF_ROOT/addons/ofxAudioFile; git pull
cd $OF_ROOT/addons/ofxPDSP; git pull

echo "DONE"
