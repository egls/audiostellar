// first open AudioStellar and check Enable in Settings > OSC
// then run this sketch

import oscP5.*;
import netP5.*;

OscP5 osc;
NetAddress remoteLocation;

String receiveAddress = "127.0.0.1";
int receivePort = 9000;

String sendAddress = "127.0.0.1";
int sendPort = 8000;

int numSounds;
int soundID = 0;
float positionX = 0;
float positionY = 0;

float threshold = 0.025; // must be a value between 0 and 1
IntList neighbors = new IntList();  


void setup() {
  
  size(800, 800);
  frameRate(25);
  
  osc = new OscP5(this, receivePort);
  remoteLocation = new NetAddress(sendAddress, sendPort);
  
  getNumSounds();

  enableGetPlayedSoundID(1); //Enable: 1 disable: 0
  
}


void draw() {
  background(0);
  
  fill(64);
  textAlign(CENTER);
  text("Click and drag to send coordinates to Audiostellar", width/2, height/2);
  
  fill(255);
  textAlign(LEFT);
  text("Number of sounds in dataset: " + numSounds, 20, 20);
  text("ID of played sound: " + soundID, 20, 40);
  text("Position of played sound: " + "X " + positionX + " Y " + positionY, 20, 60);
  
  String str = "";
  for (int i = 0; i < neighbors.size(); i++) {
   str += neighbors.get(i) + " ";
  }
  text("Neighbors of played sound: " + str, 20, 80);
}

void mouseDragged() {

  OscMessage m = new OscMessage("/sounds/play");
  m.add(mouseX / float(width)); // send X values to AudioStellar between 0.0 and 1.0
  m.add(mouseY / float(height)); // send Y values to AudioStellar between 0.0 and 1.0
  osc.send(m, remoteLocation);
}

void mouseClicked() {

  OscMessage m = new OscMessage("/sounds/play");
  m.add(mouseX / float(width)); // send X values to AudioStellar between 0.0 and 1.0
  m.add(mouseY / float(height)); // send Y values to AudioStellar between 0.0 and 1.0
  osc.send(m, remoteLocation);
}

void oscEvent(OscMessage m) {
  
  if (m.checkAddrPattern("/sounds/getNumSounds")) {
    numSounds = m.get(0).intValue();
  }

  else if (m.checkAddrPattern("/sounds/getPlayedSoundID")) {
    soundID = m.get(0).intValue();
    
    // get X and Y coordinates of played sounds;
    getPositionByID(soundID);
    
    // get IDs of played sounds;
    getNeighborsByID(soundID);
  }
  
  else if (m.checkAddrPattern("/sounds/getPositionByID")) {
    positionX = m.get(0).floatValue();
    positionY = m.get(1).floatValue();
  }
  
  else if (m.checkAddrPattern("/sounds/getNeighborsByID")) {
    neighbors.clear();
    for(int i = 0; i < m.arguments().length; i++) {
      int ID = m.get(i).intValue();
      neighbors.append(ID);
    }
  }
  
}

void getNumSounds() {
  OscMessage m = new OscMessage("/sounds/getNumSounds");
  osc.send(m, remoteLocation);
}

void enableGetPlayedSoundID (int value) {
  OscMessage m = new OscMessage("/sounds/getPlayedSoundID");
  m.add(value);
  osc.send(m, remoteLocation);
}

void getPositionByID (int ID) {
  OscMessage m = new OscMessage("/sounds/getPositionByID");
  m.add(ID);
  osc.send(m, remoteLocation);
}

void getNeighborsByID(int ID) {
  OscMessage m = new OscMessage("/sounds/getNeighborsByID");
  m.add(ID);
  m.add(threshold);
  osc.send(m, remoteLocation);
}
