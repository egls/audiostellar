# OSC Documentation

AudioStellar has a basic OSC interface that can be used to control its parameters from other aplications and devices. Traversing latent space in custom ways is also possible without having to program a whole new mode.

We are providing examples in **PureData**, **Max**, **Tidal** and **Python** of many of these functionalities.

This document provides a reference to all implemented OSC routes. Words in brackets are parameters needed. Words in double brackets are optional

## Play sounds  

#### /sounds/play [x] [y] [[volume]]

Play a sound near x,y coordinates. x and y parameters are between [0,1]. Volume is optional and is between [0,1]. A cross located at provided coordinates will briefly appear on AudioStellar's screen as a visual feedback.

#### /sounds/playX [x] [[volume]]

Play a sound near x coordinate and the last y coordinate sent. x parameter is between [0,1]. This endpoint is meant to be used for applications that can't send multiple parameters at once. Volume is optional and is between [0,1]. A cross located at provided coordinates will briefly appear on AudioStellar's screen as a visual feedback.

#### /sounds/playY [y] [[volume]]

Play a sound near y coordinate and the last x coordinate sent. y parameter is between [0,1]. This endpoint is meant to be used for applications that can't send multiple parameters at once. Volume is optional and is between [0,1]. A cross located at provided coordinates will briefly appear on AudioStellar's screen as a visual feedback.

#### /sounds/playID [sound_id] [[volume]]

Play sound with ID **sound_id**. Volume is optional and is between [0,1].

#### /sounds/playCluster [clusterName] [[index]] [[volume]]

Play a sound from a cluster named **clusterName**. If **index** is not present AudioStellar will choose a random one; note that the index will cycle through the number of sounds in the cluster. Volume is optional and is between [0,1].

#### /sounds/playClusterBySoundId [sound_id] [[index]] [[volume]]

Play a sound from the same cluster as the sound with id **sound_id**. If **index** is not present AudioStellar will choose a random one; note that the index will cycle through the number of sounds in the cluster. Volume is optional and is between [0,1].

## Get information

This routes are used to get information about currently opened dataset. After sending one of this messages, another message will arrive with the same route and with the parameters as the response.

#### /sounds/getNumSounds

Returns the number of sounds in the dataset. Available sounds ids go from 0 to the number returned by this function.

#### /sounds/getPositionByID [sound_id]

Returns x and y coordinates of the sound with id *sound_id*

#### /sounds/getNeighborsByID [sound_id] [threshold]

Returns an array of sound ids with neighbors of the sound with id *sound_id*. The threshold parameter [0,1] is up to how far a sound is considered a neighbor. It is defined as the squared distance normalized by the width of the window.

#### /sounds/getNeighborsByXY [x] [y] [threshold]

Returns an array of sound ids near coordinates x,y. The threshold parameter [0,1] is up to how far a sound is considered a neighbor. It is defined as the squared distance normalized by the width of the window.

#### /sounds/getPlayedSoundID [enabled]

Send 1 to enable or 0 to disable. AudioStellar will send an OSC message using this route with the ID of any played sound as a parameter. It is disabled by default.

#### /sounds/getClusterIDByID [sound_id]

Returns cluster ID of the sound with id *sound_id*

#### /sounds/getClusterNameByID [sound_id]

Returns cluster name of the sound with id *sound_id*

#### /sounds/getClusterNameByClusterID [cluster_id]

Returns cluster name of the cluster with id *cluster_id*

## Control parameters

This routes will change values of faders in the UI. Is is meant to be used with external applications and devices such as OSC controllers. Note that if OSC is enabled, AudioStellar will also send this routes with the current value as a parameter so both faders are in sync.  

### Particle mode

#### /modes/particle/vol [volume]

Sets volume of particle mode [0,1]

#### /modes/particle/lifespan [lifespan]

Sets lifespan of particle mode [0, 10]. Particles with a lifespan of 10 will live forever.

#### /modes/particle/emit [x] [y]

Emit particles at x,y coordinates [0,1]

#### /modes/particle/emit_x [x]

Emit particles at coordinate **x** and the last coordinate y sent. **x** parameter is between [0,1]. This endpoint is meant to be used for applications that can't send multiple parameters at once.

#### /modes/particle/emit_y [y]

Emit particles at coordinate **y** and the last coordinate x sent. **y** parameter is between [0,1]. This endpoint is meant to be used for applications that can't send multiple parameters at once.

#### /modes/particle/velocity [x] [y]

*Swarm option only*. Sets velocity **x** and **y** of particles.

#### /modes/particle/velocity_x [x]

*Swarm option only*. Sets velocity **x** of particles.

#### /modes/particle/velocity_y [y]

*Swarm option only*. Sets velocity **y** of particles.

#### /modes/particle/spread [depth]

*Swarm option only*. Sets the spread parameter of particles. [0,1]

#### /modes/particle/speed [speed]

*Explosion option only*. Sets the speed parameter of particles. [0,1]

#### /modes/particle/density [density]

*Explosion option only*. Sets the density parameter of particles. [0,100]

### Sequence mode

#### /sequencemode/vol/[track_number] [volume]

Sets the volume of track **track_number**. [0,1]
Note that in this case the parameter **track_number** is part of the route.

## Control using Tidal cycles (experimental)

Current implementation allows to control the sound, n, and gain params

Timing will be a little bit off since we don't handle the timing of events, just fires messages as soon as they arrive.

Using previously named clusters you can treat them as a folder of samples.

More info in osc_examples/osc.tidal
