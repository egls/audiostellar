﻿# Trabajo de final de grado - escrito

## Hipótesis 

Partiendo de distintas técnicas de agrupación de muestras de audio se desarrollará un instrumento musical digital que implemente formas de exploración de estas muestras agrupadas, en pos de una búsqueda de nuevos resultados estéticos relacionados al mundo del sampling y la reutilización de material sonoro preexistente.



## Marco bibliográfico

["Instrumento Musical - Experimento de Google"](https://github.com/googlecreativelab/aiexperiments-drum-machine)

["Libro sobre la historia de la visualización de datos"](www.amazon.com/Cartographies-Time-A-History-Timeline/dp/1616890584 )

["Visualizing Data. Visualización de datos aplicado al 'Creative Coding'"](http://shop.oreilly.com/product/9780596514556.do )
    
## Disparadores

El principal disparador para mi trabajo, es el trabajo de Robert Henke (alias monolake), un programador y músico alemán que trabaja desarrollando instrumentos para el software Ableton Live. 
Uno de los instrumentos que desarrolló se llama granulator, y permite trabajar con un archivo de audio, segmentarlo en partes muy pequeñas (también llamados granos) y lograr resultados tímbricos a partir de distintos recorridos de estos segmentos. Para entender un poco mejor de qué se trata, [“aquí”](https://www.youtube.com/watch?v=9pn_b7OUO6I&t=52s) un video.
Esta técnica de síntesis sonora que parte de la segmentación de audios preexistentes se llama síntesis granular. El pionero de esta técnica fue Iannis Xenakis, quien comenzó cortando cintas magnéticas con audio grabado en partes minúsculas. La síntesis granular se popularizó hacia la década del 90 cuando la capacidad de procesamiento de las computadoras permitieron trabajar con muestras de audio en tiempo real.
Lo que captó mi atención de esta técnica fue la posibilidad de obtener transformaciones tímbricas de una cierta cualidad “natural” y progresiva, como consecuencia de trabajar con registros de audio, y no sonidos generados por computadora o algún tipo de hardware. Esto es, si separamos un audio en varias muestras de, por ejemplo, una persona cantando, cada muestra contigua guardara grandes similitudes tímbricas. De esta manera, surgió el siguiente disparador: Si pudiese ubicar distintos audios de corta duración en un espacio abstracto y disponerlos en función de sus similitudes tímbricas, podría lograr un tipo de síntesis parecida, pero con un mayor rango de posibilidades ya que podría trabajar con varios audios distintos, así también generar un recorrido de estos audios ya no unidimensional sino, en principio, bidimensional.

Uno de los autores que me sirvió como punto de partida para este trabajo fue Kyle Mc. Donald, artista y programador. Él principalmente investiga nuevas herramientas digitales y cómo estas pueden servir para potenciar trabajos artísticos, y más específicamente, nuevas herramientas de composición e improvisación musical. Últimamente, sus trabajos son cercanos al mundo del machine learning, los sistemas de clasificación y el Big Data. Si bien mi trabajo no ahondará en este tipo de algoritmos, analizar estos trabajos me permite explorar distintas formas de visualizar e interactuar con grandes cuerpos de datos. 
En su artículo A return to Machine Learning, explora una serie de técnicas y algoritmos de Aprendizaje automático desarrollados en los últimos años y sus posibles aplicaciones en trabajos artísticos. El apartado Working with Archives and Libraries donde profundiza en técnicas para auto disposición de muestras de audio en función de sus características tímbricas, fue fundamental para mí para entender el estado actual de posibilidades de trabajo con grandes cantidades de muestras de audio. En particular este video me parece bastante ilustrativo del tipo de posibilidades que existen en la visualización e interacción de grandes cantidades de información sonora. 

## Estructura del trabajo
 
  * Introducción
      * Hipótesis y objetivos principales
  * Antecedentes y disparadores
    * Breve historia de la visualización de datos y su interección en el campo artístico
    * Sintesis granular y síntesis concatenativa como antecedentes en el análisis de muestras sonoras con fines artísticos.
    * Análisis de casos: interecciones entre la visualización de datos y los instrumentos músicales. El diseño de interfaces contemporáneas.
  * Desarrollo
      * Punto de partida. 
          * Qué herramientas y lenguajes están involucrados.
          * Qué límites de desarrollo se plantearon desde el inicio (que problema intenta resolver). Cuáles se mantuvieron, cuáles se modificaron. 
          * Como está desarrollada la interfaz. Que modos de operación/interacción tiene el usuario. Que posibilidades sonoras resultan. 
          * Qué problemas de desarrollo se encontraron. Como se lograron superar. 
          * Limitaciones del software. Cómo se podrían sortear a futuro. 
          * Cómo contribuir.
      * Desarrollo:  
          * Hacia una herramienta de análisis sonoro.(modos de visualización de la información obtenida) 
          * Hacia un instrumento musical. (modos de operación y performance)
  * Interacción con el proyecto de investigación que lo engloba.
      *  Conceptos básicos de machine learning y clustering. Cómo se relaciona con este trabajo.
      *  El problema de la reducción de dimensionalidad(PCA, TSNE).
      *  Qué otras formas de agrupación se podrían implementar.
  * Conclusiones y desarrollos futuros
      
