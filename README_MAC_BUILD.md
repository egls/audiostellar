# INSTRUCTIONS FOR MAC BUILD

## Compiling

1. **Clone repo** in of 0.11.x folder /apps/myApps *(advanced users can set OF_ROOT enviroment variable to point where you want instead)*
2. Run **install_addons.sh** from terminal.
3. Run **Project Generator** and import AudioStellar project. Click Generate and open in IDE (XCode).
4. Check for files with a ! sign in the file explorer left bar, all files should match with the ones in the folder. If they don't, remove the extras and add missing ones.
4. **Compile and run.** Have a look [here](https://openframeworks.cc/setup/xcode/) if you run into trouble.

## Distribution

1. Create a folder which will include the contents of the .dmg package.
    - AudioStellar.app
    - osc_examples/
    - Applications.link (this is a shortcut to make installation easy).
2. Open **Disk Utility.app**, go to **File > New Image > Image** from folder
3. Select folder created in step 1.
4. Leave encryption set to *none* and Image format to *compressed*.
5. Mount the created image.
6. Right click or ctrl+click > **Show View Options** and customise the look of the package. *You can select a background image with an arrow to instruct users to drag the .app to the Applications folder in macOS.*
7. **Code Sign** the package:
    1. Open **Keychain Access.app**
    2. Go to **Keychain Access > Certificate Assistant > Create a Certificate...**
    3. Set Name to **AudioStellar** and Identity type to **Self Signed Root**. Click Create.
    4. In terminal run:
        > sudo codesign -fs **AudioStellar** /Volumes/AudioStellar/AudioStellar.app
8. From terminal convert the .dmg into read-only package:
    > hdiutil convert AudioStellar.dmg -format UDZO -o AudioStellar_vX.XX.X.dmg
9. Package AudioStellar_vX.XX.X.dmg is ready to distribute.