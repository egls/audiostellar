#include "Sound.h"
#include "Voices.h"

ofImage Sound::imgGlow;
//ofImage Sound::imgGlowBig;

ofColor Sound::colorSelected = ofColor(232, 11, 85);
// TWEEN::Manager Sound::manager;

Sound::Sound(string _name, ofVec3f _position, int _cluster) {
    name = _name;
    position = _position;
    originalPosition = _position;
    cluster = _cluster;

    if ( !Sound::imgGlow.isAllocated() ) {
        Sound::imgGlow.load("assets/star0.png");

//        Sound::imgGlowBig.load("assets/star0.png");
//        Sound::imgGlowBig.resize(size * glowBigSize, size * glowBigSize);

        TWEEN::setTimeFunc( ofGetElapsedTimef );
    }
}
Sound::Sound(string _name) {
    name = _name;
    position.set(ofRandom(ofGetWidth() / 3, ofGetWidth() * 2 / 3),
                 ofRandom(ofGetHeight() / 3, ofGetHeight() * 2 / 3));
}
void Sound::update() {
    
    if ( Voices::getInstance()->isPlaying(this) ) {
        finishedPlaying = false;
        if (size < SIZE_ORIGINAL * 2) {
            size += sizeInc;
        }
    } else {
        if (size > SIZE_ORIGINAL) {
            size -= sizeInc;
            finishedPlaying = false;
        } else if (finishedPlaying == false) {
            finishedPlaying = true;
            if ( !hovered ) {
                stopGlow();
            }
        }
    }
}

void Sound::drawGlow() {
    if ( glowOpacity > 0 ) {
        int glowSize = size*12;
        if ( !selected ) {
//            ofSetColor(255, 255, 255, glowOpacity);
            ofSetColor( ColorPaletteGenerator::getColor(cluster), glowOpacity);
//            ofSetColor(255, 255, 255, glowOpacity);
        } else {
            ofSetColor(colorSelected.r, colorSelected.g, colorSelected.b, glowOpacity);
        }
        Sound::imgGlow.draw(position.x - glowSize / 2,
            position.y - glowSize / 2,
            glowSize,
            glowSize);
    }
}

void Sound::draw() {
    
    ofFill();
    drawGlow();

    
    if (mute) {
        ofColor clusterColor = ColorPaletteGenerator::getColor(cluster);
        ofSetColor(clusterColor, 10);
    }
    else {
        if ( !selected ) {
            ofColor clusterColor = ColorPaletteGenerator::getColor(cluster);
            clusterColor.a = floor(colorOpacity);
            ofSetColor( clusterColor );
            
            if ( clusterIsHovered ) {
                if ( colorOpacity < 255 ) {
                    colorOpacity += ofGetLastFrameTime() * 500;
                }
            } else {
                if ( colorOpacity > OPACITY_MIN ) {
                    colorOpacity -= ofGetLastFrameTime() * 500;
                }
            }
            
            colorOpacity = ofClamp(colorOpacity, 0, 255);
            
        } else {
            ofSetColor(colorSelected, 255);
        }
    }

//    debug
//    if ( id == 100 ) {
//        ofSetColor(255);
//        ofDrawBitmapString( ofToString(colorOpacity), 300,300 );
//        ofDrawBitmapString( ofToString(clusterIsHovered), 300,350 );
//    }

    ofDrawCircle(position.x, position.y, size );
}

// void Sound::mouseMoved(int x, int y) {
//     if ( getPosition().squareDistance( ofVec2f(x,y) ) <= pow(size,2) ) {
//         if ( hovered == false ) {
//             currentTween = tweenManager.addTween( glowOpacity, 255, 0.2 );
//             currentTween->start();
//
//             hovered = true;
//         }
//     } else {
//         if ( hovered == true ) {
//             if ( finishedPlaying ) {
//                 if ( currentTween != NULL ) {
//                     tweenManager.remove(currentTween);
//                 }
//                 tweenManager.addTween( glowOpacity, 0, 0.2 )->start();
//             }
//             hovered = false;
//         }
//     }
// }

void Sound::startGlow() {
    currentTween = tweenManager.addTween( glowOpacity, 100, 0.2 );
    currentTween->start();
}
void Sound::stopGlow() {
    if ( currentTween != NULL ) {
        tweenManager.remove(currentTween);
    }
    tweenManager.addTween( glowOpacity, 0, 0.2 )->start();
}
void Sound::setHovered(bool b) {
    if ( b ) {
        startGlow();
    } else {
        if ( finishedPlaying ) {
            stopGlow();
        }
    }

    hovered = b;
}
bool Sound::getHovered() {
    return hovered;
}


bool Sound::onSound(ofVec2f playerPosition) {
    if(Utils::distance(position,playerPosition) < Utils::DISTANCE_TRESHOLD) {
        return true;
    } else {
        return false;
    }
}
ofVec2f Sound::getPosition() {
    return position;
}

void Sound::setCluster(int cluster)
{
    this->cluster = cluster;
}
string Sound::getFileName() {
    return name;
}
int Sound::getCluster() {
    return cluster;
}
bool Sound::getHide() {
    return hide;
}
void Sound::setHide(bool h) {
    hide = h;
}

void Sound::toggleHide(){
    hide = !hide;
}

void Sound::useOriginalPosition() {
    position = originalPosition ;
}

void Sound::drawGui() {
    fileName = getFileName();
    Tooltip::tooltip x = { "File name", fileName.c_str() };
    Tooltip::setTooltip(x);
}

Voice * Sound::play(){
    return Voices::getInstance()->getVoice(this);
}
