#include "AudioEngine.h"
#include "Voices.h"

AudioEngine * AudioEngine::instance = nullptr;

AudioEngine * AudioEngine::getInstance() {
    if (instance == nullptr) {
        instance = new AudioEngine();
    }
    return instance;
}

AudioEngine::AudioEngine() {
    refreshDeviceList();

    //int deviceID = getDefaultAudioOutDeviceIndex();
    //setup(deviceID, selectedSampleRate, selectedBufferSize);
}

void AudioEngine::setup(int deviceID, int sampleRate, int bufferSize) {
    
    ofLog() << "calling setup";
    int numChannels = getAudioOutNumChannels(deviceID);
    
    // we should check numChannels != -1 ??
    
    if (numChannels > PDSP_MAX_OUTPUT_CHANNELS) {
        ofLogWarning() << "Selected device has " << numChannels << " output channels. Setting output channels to " << PDSP_MAX_OUTPUT_CHANNELS <<".";
        numChannels = PDSP_MAX_OUTPUT_CHANNELS;
    }
    
    // its better not to stop, since PDSP handles the change correctly
//    engine.stop();

    engine.setOutputDeviceID(deviceID);
    engine.setChannels(0, numChannels);
    engine.setup(sampleRate, bufferSize, 3);
    
    selectedAudioOutDeviceID = deviceID;
    selectedSampleRate = sampleRate;
    selectedBufferSize = bufferSize;
    selectedAudioOutNumChannels = numChannels;
    
    createOutputs();

}

void AudioEngine::stop()
{
	engine.stop();
}

void AudioEngine::selectAudioOutDevice(int deviceID) {
    
    Voices::getInstance()->reset();
    
    bool supportedSampleRate = false;
    
    // check if current sample rate is supported by the device
    for (int i = 0; i < devices[deviceID].sampleRates.size(); i++) {
        if (devices[deviceID].sampleRates[i] == selectedSampleRate) {
            supportedSampleRate = true;
            break;
        }
    }
    
    if (supportedSampleRate) {
        setup(deviceID, selectedSampleRate, selectedBufferSize);
    }
    else {
        setup(deviceID, devices[deviceID].sampleRates[0], selectedBufferSize);
    }
    
}

void AudioEngine::selectAudioOutDevice(string deviceName) {
    
    bool found = false;
    
    for (int i = 0; i < devices.size(); i++) {
        if (devices[i].name == deviceName) {
            found = true;
            selectAudioOutDevice(i);
            break;
        }
    }
    if (!found) {
        ofLogWarning () << "Audio device not found. Switching to default device.";
        selectAudioOutDevice(getDefaultAudioOutDeviceIndex());
    }
}

void AudioEngine::selectSampleRate(int sampleRate) {
    Voices::getInstance()->reset();
    setup(selectedAudioOutDeviceID, sampleRate, selectedBufferSize);
}

void AudioEngine::selectBufferSize(int bufferSize) {
    Voices::getInstance()->reset();
    setup(selectedAudioOutDeviceID, selectedSampleRate, bufferSize);
}

//void AudioEngine::start(){
//
//    //engine.setup();
//    engine.start();
//
//}

vector<ofSoundDevice> AudioEngine::listDevices()
{
    return devices;
}

void AudioEngine::refreshDeviceList()
{
    devices = engine.listDevices();

}

int AudioEngine::getDefaultAudioOutDeviceIndex() {
    
    for (int i = 0; i < devices.size(); i++) {
        if (devices[i].name == "default") {
            return i;
        }
    }

    for (int i = 0; i < devices.size(); i++) {
        if (devices[i].isDefaultOutput) {
            return i;
        }
    }
    
    return 0; //ok first will be fine... or not ? :O
}

int AudioEngine::getAudioOutNumChannels(int deviceID) {
    
    if ( deviceID < devices.size() ) {
        return devices[deviceID].outputChannels;
    }

    return -1;
}

void AudioEngine::createOutputs() {

    outputs.clear();
    
    for (int i = 0; i < selectedAudioOutNumChannels; i+=2) {
        Output output;
        output.type = "stereo";
        output.channel0 = i;
        output.channel1 = i + 1;
        output.enabled = false;
        output.label = ofToString(i + 1) + " - " + ofToString(i + 2);
        outputs.push_back(output);
    }
    for (int i = 0; i < selectedAudioOutNumChannels; i++) {
        Output output;
        output.type = "mono";
        output.channel0 = i;
        output.channel1 = i;
        output.enabled = false;
        output.label = ofToString(i + 1);
        outputs.push_back(output);
    }
    
    if (outputs.size() > 0) {
        outputs[0].enabled = true;
    }
}

Json::Value AudioEngine::save() {
    
    Json::Value root = Json::Value(Json::objectValue);
    
    root["selectedAudioOut"] = devices[selectedAudioOutDeviceID].name;
    root["selectedSampleRate"] = selectedSampleRate;
    root["selectedBufferSize"] = selectedBufferSize;
    
    return root;
}

void AudioEngine::load(Json::Value json) {
    
    Json::Value audioSettings = json["audioSettings"];
    
    if (audioSettings["selectedSampleRate"] != Json::nullValue) {
        selectedSampleRate = audioSettings["selectedSampleRate"].asInt();
    }

    if (audioSettings["selectedBufferSize"] != Json::nullValue) {
        selectedBufferSize = audioSettings["selectedBufferSize"].asInt();
    }
    
    if (audioSettings["selectedAudioOut"] != Json::nullValue) {
        string selectedAudioOut = audioSettings["selectedAudioOut"].asString();
        selectAudioOutDevice(selectedAudioOut);
    } else {
        int deviceID = getDefaultAudioOutDeviceIndex();
        setup(deviceID, selectedSampleRate, selectedBufferSize);
    }
}

void AudioEngine::exit() {
    engine.stop();
}
