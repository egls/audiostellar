#include "Voices.h"

Voices* Voices::instance = nullptr;

Voices::Voices() {
    audioEngine = AudioEngine::getInstance();
}

Voices *Voices::getInstance() {
    if ( instance == nullptr ) {
        instance = new Voices();
    }
    return instance;
}

void Voices::masterVolumeListener(float &v) {
    // WORK-AROUND if we set 0 to amp it will freeze our samples
    if ( v == 0.0f ) {
        v = 0.00001f;
    }
    v >> out0.in_mod();
    v >> out1.in_mod();
}

void Voices::setup(int numVoices) {
    
    this->numVoices = numVoices;
    
    voices.resize(numVoices);
    
    loadedVoices = 0;
    
    for (int i = 0; i < voices.size(); ++i) {
        voices[i].setup();
    }

    // init voice map
    vector <Sound *> sounds = Sounds::getInstance()->getSounds();
    for(int i = 0; i < sounds.size(); i++) {
        voiceMap[ sounds[i] ] = vector <Voice *>();
    }

    masterVolume.addListener(this, &Voices::masterVolumeListener);
    masterVolume.set("Master volume", DEFAULT_VOLUME);
}

void Voices::reset() {
    
    for (int i = 0; i < voices.size(); i ++) {
        voices[i].stop();
        voices[i].unload();
    }
    voices.clear();
    voiceMap.clear();
    setup(numVoices);
}

void Voices::playSound(Sound * sound, float volume) {
    
    int channel0 = sound->channel0;
    int channel1 = sound->channel1;
    
    if (!sound->mute) {
        
        Voice * voice = getVoice(sound);
        
        voice->setOutputs(channel0, channel1);
        voice->play(volume);
    }
}

Voice * Voices::getNonPlayingVoice(Sound * sound) {
    for (int i = 0; i < voiceMap[sound].size(); i++) {
        if ( !voiceMap[sound][i]->isPlaying() ) { // voice not playing
            return voiceMap[sound][i];
        }
    }
    return nullptr;
}

Voice * Voices::getFreeVoice() {
    
    if (loadedVoices < numVoices) {
        Voice * voice = &voices[loadedVoices];
        loadedVoices++;
        return voice;
    }
    else {
        for (int i = 0; i < voices.size(); i++) {
            if ( !voices[i].isPlaying() ) {
                return &voices[i];
            }
        }
    }
    return nullptr;
}

Voice * Voices::getReplaceableVoice() {
    float playerPosition = 0;
    Voice * voice;
    for (int i = 0; i < voices.size(); i++) {
        if (voices[i].getPlayerPosition() > playerPosition) {
            playerPosition = voices[i].getPlayerPosition();
            voice = &voices[i];

        }
    }
    return voice;
}

bool Voices::isPlaying(Sound * sound) {
    bool isPlaying = false;
    for (int i = 0; i < voiceMap[sound].size(); i++) {
        if ( voiceMap[sound][i]->isPlaying() ) {
            isPlaying = true;
        }
    }
    return isPlaying;
}

Voice *Voices::getVoice(Sound *sound)
{
    Voice * voice;

    voice = getNonPlayingVoice(sound);
    if (voice == nullptr) { // no voice allocated or all allocated voices are playing
        voice = getFreeVoice();
        if (voice == nullptr) { // no free voice, all voices are playing
            voice = getReplaceableVoice();
            takeOverVoice(sound, voice);
        }
        else { // voice not playing found
            takeOverVoice(sound, voice);
        }
    }

    return voice;
}

void Voices::takeOverVoice(Sound * sound, Voice * voice) {
    if (voice->sound != nullptr) {
        vector <Voice *> * voices = &voiceMap[voice->sound];
        voices->erase(remove(voices->begin(), voices->end(), voice), voices->end());
    }
    voiceMap[sound].push_back(voice);
    voice->sound = sound;
    voice->load( sound->getFileName() );
}

void Voice::setup() {
    
    addModuleOutput( "0", volume0 );
    addModuleOutput( "1", volume1 );

    addModuleInput("speed", sampler0.in_pitch());
    addModuleInput("speed", sampler1.in_pitch());

    sampleTrigger >> sampler0 >> volume0;
    sampleTrigger >> sampler1 >> volume1;
    
    envTrigger >> env >> amp0.in_mod();
    env >> amp1.in_mod();
    
    sampler0.addSample( &sample, 0 );
    sampler1.addSample( &sample, 1 );
    
    volume0.set(0.0f);
    volume1.set(0.0f);

    //    sample.setVerbose(true);
}

Json::Value Voices::save() {
   return new Json::Value;
}

void Voice::load(string path) {
    stop();
    sample.load(path);
    if (sample.channels == 1) {
        sampler1.setSample(&sample, 0, 0);
    } else {
        sampler1.setSample(&sample, 0, 1);
    }
}

void Voice::unload() {
    stop();
    sample.unLoad();
}

void Voice::play(float volume) {
    setVolume(volume);
    envTrigger.trigger(1.0f);
    sampleTrigger.trigger(1.0f);
}

void Voice::stop() {
    setVolume(0.0f);
    envTrigger.off();
    sampleTrigger.off();
}

bool Voice::isLoaded() {
    return sample.loaded();
}

bool Voice::isPlaying() {
    return (sampler0.meter_position() != 0.0f) &&
           (sampler0.meter_position() <= 1.0f);
}

float Voice::getPlayerPosition() {
    return sampler0.meter_position();
}

void Voice::setVolume(float volume) {
    volume0.set(volume);
    volume1.set(volume);
}

void Voice::setOutputs(int channel0, int channel1) {
    
    out("0").disconnectOut();
    out("1").disconnectOut();
    
    if ( channel0 == 0 && channel1 == 1 ) {
        out("0") >> Voices::getInstance()->out0 >> audioEngine->engine.audio_out(channel0);
        out("1") >> Voices::getInstance()->out1 >> audioEngine->engine.audio_out(channel1);
    } else {
        out("0") >> audioEngine->engine.audio_out(channel0);
        out("1") >> audioEngine->engine.audio_out(channel1);
    }
}

