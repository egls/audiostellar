#pragma once

#include "ofMain.h"
#include "ofxImGui.h"
#include "SessionManager.h"
#include "Utils.h"
#include "Sound.h"
#include "Utils/UI.h"
#include "Modes/Modes.h"
#include "CamZoomAndPan.h"
#include "MidiServer.h"
#include "DBScan.h"
#include "ColorPaletteGenerator.h"
#include "AudioDimensionalityReduction.h"
#include "ofxOsc.h"
#include "OscServer.h"
#include "Voices.h"

class ofApp : public ofBaseApp {
public:
    void setup();
    void update();
    void draw();
    void exit();

    void drawFps();

    void keyPressed(ofKeyEventArgs & e);
    void keyReleased(int key);
    void mousePressed(int x, int y, int button);
    void mouseScrolled(int x, int y, float scrollX, float scrollY);
    void mouseDragged(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseMoved(int x, int y);

    char cwd[1024];
    vector<string> arguments;

    SessionManager * sessionManager = NULL;
    Sounds * sounds = NULL;
    MidiServer * midiServer = NULL;
    OscServer * oscServer = NULL;
    Modes * modes = NULL;
    Gui * gui = NULL;
    Gui * startScreen = NULL;

    ofxJSONElement jsonFile;

    CamZoomAndPan cam;
    bool useCam = true;

};
