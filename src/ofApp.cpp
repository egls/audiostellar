#include "ofApp.h"
#include "Voices.h"
//--------------------------------------------------------------
void ofApp::setup() {
    #ifdef TARGET_OS_MAC
        ofSetDataPathRoot("../Resources/data/");
    #endif

    ofSetWindowTitle("AudioStellar");
    ofSetEscapeQuitsApp(false);

    ofEnableAlphaBlending();
    ofSetVerticalSync(true);

    sounds = Sounds::getInstance();
    oscServer = OscServer::getInstance();
    midiServer = new MidiServer();
    modes = new Modes(midiServer);
    sessionManager = new SessionManager(modes, midiServer);
    gui = Gui::getInstance(midiServer,  modes, sessionManager, oscServer);

    midiServer->init(modes);
    cam.init();

    Voices::getInstance()->setup( Voices::getInstance()->numVoices );

    sessionManager->loadInitSession();
}

//--------------------------------------------------------------
void ofApp::update() {
    sounds->update();
    modes->update();
    oscServer->update();
}

//--------------------------------------------------------------
void ofApp::draw() {
    ofBackground(25);

    if ( useCam ) {
        cam.begin();
    }

    modes->beforeDraw();
    sounds->draw();
    modes->draw();

    if ( useCam ) {
        cam.end();
    }

    gui->draw();
}

void ofApp::keyPressed(ofKeyEventArgs & e) {
    gui->keyPressed(e);

    // in 1.0 we should rethink this design
    modes->keyPressed(e);
    sessionManager->keyPressed(e);
}

void ofApp::mousePressed(int x, int y, int button) {
    ofVec3f realCoordinates = cam.screenToWorld(ofVec3f(x,y,0));
    if(!gui->isMouseHoveringGUI()){
        modes->mousePressed( realCoordinates.x,
                             realCoordinates.y,
                             button );
        sounds->mousePressed(realCoordinates.x,
                           realCoordinates.y,
                           false);
    }
    gui->mousePressed(realCoordinates, button);
}

void ofApp::mouseMoved(int x, int y) {
    if(sessionManager->getSessionLoaded()){
        ofVec3f realCoordinates = cam.screenToWorld(ofVec3f(x,y,0));
        if(!gui->isMouseHoveringGUI()){
            sounds->mouseMoved(realCoordinates.x,
                               realCoordinates.y,
                               false);
        }
    }
}

void ofApp::mouseDragged(int x, int y, int button) {
    if(!gui->isMouseHoveringGUI()){
        ofVec3f realCoordinates = cam.screenToWorld(ofVec3f(x,y,0));
        modes->mouseDragged(realCoordinates.x,
                            realCoordinates.y,
                            button);

        sounds->mouseDragged(realCoordinates, button);
    }

}

void ofApp::mouseReleased(int x, int y, int button) {
    ofVec3f realCoordinates = cam.screenToWorld(ofVec3f(x,y,0));

    //changing this order will cause sound context menu to be shown after sound moved
    gui->mouseReleased(realCoordinates, button);

    if(!gui->isMouseHoveringGUI()){
        modes->mouseReleased(realCoordinates.x,
                             realCoordinates.y,
                             button);
        sounds->mouseReleased(realCoordinates.x,
                              realCoordinates.y,
                              button);
    }

}

void ofApp::keyReleased(int key) {
    sounds->keyReleased(key);

}
void ofApp::mouseScrolled(int x, int y, float scrollX, float scrollY) {
    // cam.mouseScrolled(x,y,scrollX,scrollY);
}

void ofApp::exit(){
    sessionManager->exit();
}



