#include "SequenceMode.h"

int SequenceMode::currentBeat;
bool SequenceMode::isModeActive = false;

SequenceMode::SequenceMode(Sounds *_sounds, MidiServer *_midiServer) {
    sounds = _sounds;
    midiServer = _midiServer;
    sounds->hoveredActivated = true;

    modeName = "Sequence Mode";
//    iconPath = "assets/icons/sequence_mode.png";
    iconPath = "assets/icons/sequence_mode_tinteable.png";
    iconPathActive = "assets/icons/sequence_mode_active.png";

    SequenceModeTrack::sounds = _sounds;

    currentBeat = -1;
    int milis = (( 60.0 / DEFAULT_TEMPO ) * 1000);
    tempo.setup(milis/4); //semicorcheas
    tempo.start(true);
    ofAddListener( tempo.TIMER_COMPLETE, this, &SequenceMode::onTempo ) ;

    selectedTrack = &tracks[0];

    ofAddListener(OscServer::oscEvent, this, &SequenceMode::oscDispatcher);
}

Json::Value SequenceMode::save() {
    Json::Value root = Json::Value( Json::objectValue );
    Json::Value sequences = Json::Value( Json::arrayValue );

    for ( int i = 0 ; i < QTY_TRACKS ; i++ ) {
        SequenceModeTrack * track = &tracks[i];
        Json::Value sequence = Json::Value( Json::objectValue );
        Json::Value ids = Json::Value( Json::arrayValue );

        for ( int j = 0 ; j < track->secuencia.size() ; j++ ) {
            //            sequence.append( track->secuencia[j]->getFileName() );
            ids.append( track->secuencia[j]->id );
        }

        sequence["ids"] = ids;
        sequence["offset"] = track->offset.get();
        sequence["probability"] = track->probability;
        sequence["bars"] = track->selectedUnit.get();

        sequences.append(sequence);
    }

    root["sequences"] = sequences;
    return root;
}
void SequenceMode::load(Json::Value jsonData) {
    Json::Value sequences = jsonData["sequences"];
    if ( sequences != Json::nullValue ) {
        for ( int i = 0 ; i < sequences.size() ; i++ ) {
            Json::Value sequence = sequences[i];
            bool oldFormat = false; //support old format

            if ( sequence.size() > 0 ) {
                try {
                    if ( sequence[0].type() == Json::intValue ) {
                        oldFormat = true;
                    }
                } catch (std::runtime_error a) {
                    // do nothing, oldFormat is false
                }

                if ( oldFormat ) {
                    for ( int j = 0 ; j < sequence.size() ; j++ ) {
                        tracks[i].toggleSound( sounds->getSoundById( sequence[j].asInt() ), false);
                    }
                } else {
                    Json::Value ids = sequence["ids"];

                    for ( int j = 0 ; j < ids.size() ; j++ ) {
                        tracks[i].toggleSound( sounds->getSoundById( ids[j].asInt() ), false );
                    }
                }

                if ( !oldFormat ) {
                    tracks[i].offset = sequence["offset"].asInt();
                    tracks[i].probability = sequence["probability"].asFloat();
                    tracks[i].selectedUnit = sequence["bars"].asInt();
                } else {
                    tracks[i].processSequence();
                }

                if ( tracks[i].secuencia.size() > 0 ) {
                    tracks[i].volume = 0;
                    OscServer::sendMessage("/sequencemode/vol/" + ofToString(i+1) + "/", tracks[i].volume);
                } else {
                    if ( i < QTY_TRACKS ) {
                        tracks[i].volume = 1;
                        OscServer::sendMessage("/sequencemode/vol/" + ofToString(i+1) + "/", tracks[i].volume);
                    }
                }
            }
        }

        sounds->allSoundsSelectedOff();

        selectedTrack = &tracks[0];
        selectedTrack->selectAllSounds();
    }
}

void SequenceMode::onSelectedMode() {
    SequenceMode::isModeActive = true;

    if ( selectedTrack != NULL ) {
        selectedTrack->selectAllSounds();
    }
}
void SequenceMode::onUnselectedMode() {
    SequenceMode::isModeActive = false;

    sounds->allSoundsSelectedOff();
}

void SequenceMode::update() {
    tempo.update();
}
void SequenceMode::draw() {
    // for ( unsigned int i = 0 ; i < QTY_TRACKS ; i++ ) {
    //     tracks[i].draw( &tracks[i] != selectedTrack );
    // }
}

void SequenceMode::drawGui()
{
    auto mainSettings = ofxImGui::Settings();

    //    if(ImGui::TreeNode(modeName.c_str())) {
    if(ofxImGui::BeginTree(modeName, mainSettings) ) {
        //Bpm button
        int negra = SequenceMode::currentBeat / 4 + 1;
        ImGui::Text( ofToString(negra).c_str() );

        ImGui::SameLine();

        if ( midiServer->SliderInt( "BPM", &bpm, 40, 250 ) ) {
            int milis = (( 60.0 / bpm ) * 1000.0);
            tempo.setup( milis / 4.0);
        }
        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SEQUENCE_BPM);

        if ( ImGui::Checkbox("Use MIDI clock", &useMidiClock) ) {
            if (midiServer->getCurrentMidiDevice() == "Not set") {
                useMidiClock = false;
                Gui::getInstance()->showMessage(
                                                "No MIDI device selected"
                                                ".\n\n"
                                                "Please make sure your device is connected and select it in Settings menu.",
                                                "Warning" );
            }
            else {
                if ( useMidiClock ) {
                    tempo.stop();
                } else {
                    tempo.start(true);
                }
            }
        }
        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::MIDI_CLOCK);

        ImGui::Separator();

        ImGui::BeginChild("tracks",
                          ImVec2(0, SLIDER_VOLUME_HEIGHT + 55),
                          true,
                          ImGuiWindowFlags_HorizontalScrollbar
                          );

        for ( auto i = 0 ; i < QTY_TRACKS ; i++ ) {
            ImGui::BeginGroup();

            //Select Button
            ofColor buttonColor = ofColor::gray;

//            if ( tracks[i].secuencia.size() > 0 ) {
//                buttonColor = ofColor::lightGreen;
//            }

            if ( selectedTrack != NULL && selectedTrack == &tracks[i] ) {
                buttonColor = ofColor::orange;
            }
            ImGui::PushStyleColor(ImGuiCol_Button, buttonColor);
            ImGui::PushID( ("selectButton" + ofToString(i)).c_str() );
            if ( ImGui::Button(" ") ) {
                selectTrack( &tracks[i] );
            }
            
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SEQUENCE_ACTIVE);

            ImGui::PopID();
            ImGui::PopStyleColor();
            ///////////////

//            //Play Button
//            buttonColor = ofColor::gray;
//            if ( tracks[i].playing ) {
//                buttonColor = ofColor::green;
//            }
//            ImGui::PushStyleColor(ImGuiCol_Button, buttonColor);
//            ImGui::PushID( ("playButton" + ofToString(i)).c_str() );
//            if ( ImGui::Button(">") ) {
//                tracks[i].playing = !tracks[i].playing;
//            }
//            ImGui::PopID();
//            ImGui::PopStyleColor();
//            ///////////////

            if ( midiServer->VSliderFloat(
                     ("##slider" + ofToString(i)).c_str() ,
                     ImVec2(14,SLIDER_VOLUME_HEIGHT),
                     &tracks[i].volume,
                     0.0f, 1.0f, "") ) {

                selectTrack(&tracks[i]);
                OscServer::sendMessage("/sequencemode/vol/" + ofToString(i+1) + "/", tracks[i].volume);

            }
            
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SEQUENCE_VOLUME);

            ImGui::EndGroup();
            ImGui::SameLine();
        }
        ImGui::EndChild();

        if ( selectedTrack != NULL ) {
            ImGui::Separator();

            ImGui::Text("Selected sequence track config:");

            ofxImGui::AddParameter( selectedTrack->offset );
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SEQUENCE_OFFSET);

            ImGui::SliderFloat("Probability", &selectedTrack->probability, 0, 1 );
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SEQUENCE_PROBABILITY);

            ofxImGui::AddCombo(selectedTrack->selectedUnit, strUnits);
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SEQUENCE_BARS);

            if ( selectedTrack->secuencia.size() > 0 ) {
                
                if ( ImGui::Button("Clear sequence") ) {
                    selectedTrack->clearSequence();
                }
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SEQUENCE_CLEAR);
            }

        }

        ofxImGui::EndTree(mainSettings);
    }
}
void SequenceMode::beforeDraw() {
    for ( unsigned int i = 0 ; i < QTY_TRACKS ; i++ ) {
        tracks[i].draw( &tracks[i] != selectedTrack );
    }
}

void SequenceMode::onTempo(int &args) {
    currentBeat++;

    // tempo is a 16th note
    if ( currentBeat > 15 ) {
        currentBeat = 0;
    }
//    if ( selectedTrack != NULL ) {
//        if ( SequenceMode::currentBeat % 4 == 0 ) {
//            click = true;
//        } else {
//            click = false;
//        }
//    }

//    ofLog() << "Beat: " << currentBeat;

    for ( unsigned int i = 0 ; i < QTY_TRACKS ; i++ ) {
        tracks[i].onTempo();
    }
}

void SequenceMode::mousePressed(ofVec2f p, int button) {
    Sound * hoveredSound = sounds->getHoveredSound();
    if ( hoveredSound != NULL ) {
        if ( button == 0 ) {
            selectedTrack->toggleSound( hoveredSound );
        }
    }
}

void SequenceMode::mouseDragged(ofVec2f p, int button) {
    if ( button == 2 ) {
        Sound * hoveredSound = sounds->getHoveredSound();

        if ( hoveredSound != NULL ) {
            selectedTrack->processSequence(); //habria que poner un threshold capaz ?
        }
    }
}

void SequenceMode::midiMessage(Utils::midiMsg m)
{
    if ( m.status == "Time Clock" || m.status == "Song Pos" ) {
        if ( useMidiClock ) {
//            ofLog() << m.beats;
            int args = 0;
            currentBeat = m.beats - 1;
            onTempo(args);
        }
    }
}

void SequenceMode::oscDispatcher(ofxOscMessage &m)
{
    if( ofIsStringInString(m.getAddress(), "sequencemode") ) {
        std::vector<std::string> parts = ofSplitString(m.getAddress(), "/");

        if ( parts.size() >= 3 ) {
            if ( parts[2] == "vol" ) {
                int faderNumber = ofToInt(parts[3]) - 1;
                if ( faderNumber < QTY_TRACKS ) {
                    float vol = m.getArgAsFloat(0);
                    if ( vol >= 0 && vol <= 1 ) {
                        tracks[faderNumber].volume = vol;
                    }
                }
            } else if ( parts[2] == "mute" ) {
//                tracks[faderNumber].volume = 0;
            }
        }


    }
}

void SequenceMode::selectTrack(SequenceModeTrack *track)
{
    if ( track == selectedTrack ) {
        return;
    }

    selectedTrack = track;

    for ( auto i = 0 ; i < QTY_TRACKS ; i++ ) {
        if ( selectedTrack != &tracks[i] ) {
            tracks[i].unselectAllSounds();
        }
    }

    selectedTrack->selectAllSounds();
}

void SequenceMode::reset()
{
    for ( int i = 0 ; i < QTY_TRACKS ; i++ ) {
        tracks[i].clearSequence();
        OscServer::sendMessage("/sequencemode/vol/" + ofToString(i+1) + "/", 1.0f);
    }
};
