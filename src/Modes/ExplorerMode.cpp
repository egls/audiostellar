#include "ExplorerMode.h"

ExplorerMode::ExplorerMode(Sounds *_sounds) {
    //gui = _gui;
    sounds = _sounds;
    sounds->hoveredActivated = true;
    modeName = "Explorer Mode";
    
    //    iconPath = "assets/icons/explorer_mode.png";
    iconPath = "assets/icons/explorer_mode_tinteable.png";
    iconPathActive = "assets/icons/explorer_mode_active.png";
    // setupGui();
}
void ExplorerMode::update()
{
    //esto es así porque no se puede reproducir un Sound * desde midiMessage (threads capaz)
    if ( midiTriggeredSound ) {
        sounds->playSound(midiTriggeredSound);
        midiTriggeredSound = nullptr;
    }
}

void ExplorerMode::reset(){
    if(!midiMappings.empty()){
        midiMappings.clear();
    }
}

void ExplorerMode::mousePressed(ofVec2f p, int button) {
    Sound * hoveredSound = sounds->getHoveredSound();
    if ( hoveredSound != NULL ) {
        if ( button == 0 ) {
            sounds->playSound( hoveredSound );
        }
        
        if (button == 2) {
            showContextMenu = true;
            hoveredSoundPath = hoveredSound->getFileName();
        }
        else showContextMenu = false;
    }
    else showContextMenu = false;

}

void ExplorerMode::midiMessage(Utils::midiMsg m)
{
    
    if ( m.status == "Note On" ) {
        
        if ( MidiServer::midiLearn && sounds->lastPlayedSound != nullptr ) {
            midiMappings[ m.pitch ] = sounds->lastPlayedSound->id;
            MidiServer::midiLearn = false;
        }
        
        if ( midiMappings.find(m.pitch) != midiMappings.end() ) {
            midiTriggeredSound = sounds->getSoundById( midiMappings[ m.pitch ] );
        }
    }
}

Json::Value ExplorerMode::save()
{
    Json::Value root = Json::Value( Json::objectValue );
    Json::Value jsonMidiMappings = Json::Value( Json::objectValue );
    
    for ( auto midiMapping : midiMappings) {
        jsonMidiMappings[ ofToString(midiMapping.first) ] = midiMapping.second;
    }
    
    root["midiMappings"] = jsonMidiMappings;
    return root;
}

void ExplorerMode::load(Json::Value jsonData)
{
    Json::Value jsonMidiMappings = jsonData["midiMappings"];
    if ( jsonMidiMappings != Json::nullValue ) {
        for( Json::Value::iterator itr = jsonMidiMappings.begin() ; itr != jsonMidiMappings.end() ; itr++ ) {
            ofLog() << itr.key() << ":" << *itr;
            midiMappings[ ofToInt( itr.key().asString() ) ] = (*itr).asInt();
        }
    }
}

void ExplorerMode::drawGui() {
    
//    if (showContextMenu) {
//        
//        string buttonText;
//        string command;
//        
//        #ifdef TARGET_WIN32
//        buttonText = "Show in Explorer";
//        command = "explorer /select," + hoveredSoundPath;
//        #endif
//        
//        #ifdef TARGET_OSX
//        buttonText = "Show in Finder";
//        command = "open -R " + hoveredSoundPath;
//        #endif
//        
//        #ifdef TARGET_LINUX
//        buttonText = "Show in File Manager";
//        string hoveredSoundEnclosingDirectory = ofFilePath::getEnclosingDirectory(hoveredSoundPath);
//        string script {
//            "file_manager=$(xdg-mime query default inode/directory | sed 's/.desktop//g'); "
//            "if [ \"$file_manager\" = \"nautilus\" ] || [ \"$file_manager\" = \"dolphin\" ] || [ \"$file_manager\" = \"nemo\" ]; "
//            "then command=\"$file_manager %s\"; "
//            "else command=\"xdg-open %s\"; "
//            "fi; "
//            "$command"
//        };
//        
//        char buffer[ script.size() + hoveredSoundPath.size() + hoveredSoundEnclosingDirectory.size() ];
//        std::sprintf( buffer, script.c_str(), hoveredSoundPath.c_str(), hoveredSoundEnclosingDirectory.c_str() );
//        command = buffer;
//        #endif
//        
//        ImGui::OpenPopup("menu");
//        if (ImGui::BeginPopup("menu")) {
//            isContextMenuHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem
//                                                          | ImGuiHoveredFlags_AllowWhenBlockedByPopup
//                                                          | ImGuiHoveredFlags_ChildWindows);
//            
//            if (isContextMenuHovered) {
//                ImGui::PushStyleVar(ImGuiStyleVar_Alpha, 1.f);
//            }
//            else {
//                ImGui::PushStyleVar(ImGuiStyleVar_Alpha, .7f);
//            }
//            
//            ImGui::Text("File name");
//            if (ImGui::Button(buttonText.c_str())) {
//                ofSystem(command);
//                ImGui::CloseCurrentPopup();
//                showContextMenu = false;
//                isContextMenuHovered = false;
//            }
//            ImGui::PopStyleVar();
//            ImGui::EndPopup();
//        }
//    }
}

bool ExplorerMode::isMouseHoveringGui() {
    return isContextMenuHovered;
}
