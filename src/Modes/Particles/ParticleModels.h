#ifndef _PARTICLE_MODELS
#define _PARTICLE_MODELS

#include "ofMain.h"
#include "Particle.h"
#include "ofxImGui.h"
#include "MidiServer.h"
#include "Tooltip.h"

class MidiServer;

class SimpleParticle: public Particle {
    public:
        static string modelName;

        ofVec2f velocity;
        static ofVec2f acceleration;

        static void drawGui(MidiServer *midiServer);

        SimpleParticle();
        void customUpdate();


};

class SpreadedParticle: public Particle {
    public:
        static string modelName;

        ofVec2f velocity;
        static ofVec2f acceleration;
        static float spreadAmt;

        static void drawGui(MidiServer *midiServer);

        SpreadedParticle();
        void customUpdate();
    
    
        static const string OSC_PARTICLE_SWARM_VEL;
        static const string OSC_PARTICLE_SWARM_VEL_X;
        static const string OSC_PARTICLE_SWARM_VEL_Y;
        static const string OSC_PARTICLE_SWARM_SPREAD;



};

class RadialParticle: public Particle {
    public:
        static string modelName;

        ofVec2f velocity;
        ofVec2f acceleration;
        static float speed;
        static int density;

        static void drawGui(MidiServer *midiServer);

        RadialParticle();
        void customUpdate();
    
        static const string OSC_PARTICLE_EXPLOSION_SPEED;
        static const string OSC_PARTICLE_EXPLOSION_DENSITY;
    
};
#endif
/*
class AttractedParticle;

class AttractorParticle: public Particle{

    public:

        static string modelName;
        static int mass;
        static float gravity;

        static void drawGui();

        AttractorParticle();
        void customUpdate();
        void customDraw();
        ofVec2f attract(Particle *p);

};

class AttractedParticle: public Particle{

    public:

        static string modelName;

        ofVec2f velocity;
        ofVec2f acceleration;
        static int mass;

        static void drawGui();

        AttractedParticle();
        void customUpdate();
        void applyForce(ofVec2f force);
};
*/
