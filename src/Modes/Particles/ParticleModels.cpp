#include "ParticleModels.h"

/* INIT OF STATIC VARS  FOR EACH MODEL */

//Simple
string SimpleParticle::modelName = "Simple";
ofVec2f SimpleParticle::acceleration = ofVec2f(0,0);

//Spreaded
string SpreadedParticle::modelName = "Swarm";
ofVec2f SpreadedParticle::acceleration = ofVec2f(0,0);
float SpreadedParticle::spreadAmt = 0.9f;

//Radial
string RadialParticle::modelName = "Explosion";
float RadialParticle::speed = 0.5f;
int RadialParticle::density = 20;

const string SpreadedParticle::OSC_PARTICLE_SWARM_VEL = "/modes/particle/velocity";
const string SpreadedParticle::OSC_PARTICLE_SWARM_VEL_X = "/modes/particle/velocity_x";
const string SpreadedParticle::OSC_PARTICLE_SWARM_VEL_Y = "/modes/particle/velocity_y";
const string SpreadedParticle::OSC_PARTICLE_SWARM_SPREAD = "/modes/particle/spread";
const string RadialParticle::OSC_PARTICLE_EXPLOSION_DENSITY = "/modes/particle/density";
const string RadialParticle::OSC_PARTICLE_EXPLOSION_SPEED = "/modes/particle/speed";


//Attractor
/*
string AttractorParticle::modelName = "Attractor";
int AttractorParticle::mass = 5;
float AttractorParticle::gravity = 1.0;


string AttractedParticle::modelName = "Attractor";
int AttractedParticle::mass = 2;
*/



//Simple Model
SimpleParticle::SimpleParticle() {
    velocity = ofVec2f(0,0);

}


void SimpleParticle::customUpdate(){

    velocity += acceleration;
    position += velocity;

}


void SimpleParticle::drawGui(MidiServer *midiServer){

    ImGui::Text("Acceleration");
    ImGui::PushItemWidth(150);

    midiServer->SliderFloat("X", &acceleration.x, -1.0f, 1.0f);
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::PARTICLE_SIMPLE_X_ACCELERATION);

    ImGui::SameLine();

    midiServer->SliderFloat("Y", &acceleration.y, -1.0f, 1.0f);
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::PARTICLE_SIMPLE_Y_ACCELERATION);

    ImGui::PopItemWidth();

}

//Spreaded Model
SpreadedParticle::SpreadedParticle() {
    velocity = ofVec2f(0,0);
}

void SpreadedParticle::customUpdate() {
    //si el valor random es igual para x e y siempre se va a mover en diagonal...

    //OPCIÓN 1: Se acelera un toque más :) - No hay effecto jiggle :(
//    velocity.x += spreadAmt * ofRandom(-1.0,1.0);
//    velocity.y += spreadAmt * ofRandom(-1.0,1.0);

    //OPCIÓN 2: Effecto jiggle :) - La aceleración no funciona igual que en el modelo simple :(
    velocity.x = spreadAmt * ofRandom(-1.0,1.0) + acceleration.x;
    velocity.y = spreadAmt * ofRandom(-1.0,1.0) - acceleration.y;

    position += velocity;

}

void SpreadedParticle::drawGui(MidiServer *midiServer){
    ImGui::Text("Velocity");
    ImGui::PushItemWidth(150);
    

    if(midiServer->SliderFloat("X", &acceleration.x, -1.0f, 1.0f)) {
        OscServer::sendMessage(OSC_PARTICLE_SWARM_VEL_X, acceleration.x);
        ofxOscMessage message;
        message.setAddress(OSC_PARTICLE_SWARM_VEL);
        message.addFloatArg(acceleration.x);
        message.addFloatArg(acceleration.y);
        OscServer::sendMessage(message);
    }
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::PARTICLE_SWARM_X_ACCELERATION);

    ImGui::SameLine();

    if(midiServer->SliderFloat("Y", &acceleration.y, -1.0f, 1.0f)) {
        OscServer::sendMessage(OSC_PARTICLE_SWARM_VEL_Y, acceleration.y);
        ofxOscMessage message;
        message.setAddress(OSC_PARTICLE_SWARM_VEL);
        message.addFloatArg(acceleration.x);
        message.addFloatArg(acceleration.y);
        OscServer::sendMessage(message);
    }
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::PARTICLE_SWARM_Y_ACCELERATION);

    ImGui::PopItemWidth();

    if(midiServer->SliderFloat("Spread Amount", &spreadAmt, 0.0f, 1.0f)) {
        OscServer::sendMessage(OSC_PARTICLE_SWARM_SPREAD, spreadAmt);
    }
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::PARTICLE_SPREAD_AMOUNT);
    

}
//RadialModel
RadialParticle::RadialParticle(){
    velocity.x = ofRandom(-1,1);
    velocity.y = ofRandom(-1,1);
    acceleration.x = velocity.x * speed;
    acceleration.y = velocity.y * speed;

}
void RadialParticle::customUpdate(){

   velocity += acceleration;
   position += velocity;

}

void RadialParticle::drawGui(MidiServer *midiServer){
    
    if(midiServer->SliderFloat("Speed", &speed, 0.0f, 1.0f)) {
        OscServer::sendMessage(OSC_PARTICLE_EXPLOSION_SPEED, speed);
    }
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::PARTICLE_EXPLOSION_SPEED);

    if(midiServer->SliderInt("Density", &density, 0, 100)) {
        OscServer::sendMessage(OSC_PARTICLE_EXPLOSION_DENSITY, density);
    }
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::PARTICLE_EXPLOSION_DENSITY);

}

/*
//Attractor Model
AttractorParticle::AttractorParticle(){
    size = 40;
    age = 10;
    mass = 5;

}

void AttractorParticle::customUpdate(){


}

void AttractorParticle::customDraw(){
   ofNoFill();
   ofSetLineWidth(2);

   float dens = 3.5;
   int coreRadius = 5;

   ofColor cyan = ofColor::cyan;

   for(float i = size; i > coreRadius; i-=dens){

        ofSetColor(cyan, ofMap(i, size, coreRadius, 10, 255));
        ofDrawCircle(position.x, position.y, i);
   }

}

void AttractorParticle::drawGui(){
    ImGui::SliderFloat("Gravity", &gravity, 0.01f, 20.0f);
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::ATTRACTOR_GRAVITY);
    
    ImGui::SliderInt("Attractor Mass", &mass, 1, 100);
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::ATTRACTOR_MASS);

}

ofVec2f AttractorParticle::attract(Particle *p){

   ofVec2f f = position - p->position;
   float distance = f.length();
   f.normalize();
   float strength = (gravity * mass * p->mass) / (distance * distance);

   f *= strength;

   return f;
}

AttractedParticle::AttractedParticle(){
    mass = 2;
    age = 10;
}

void AttractedParticle::drawGui(){
    ImGui::SliderInt("Particle Mass", &mass, 1, 100);

}


void AttractedParticle::customUpdate(){

    velocity += acceleration;
    position += velocity;
}

void AttractedParticle::applyForce(ofVec2f force){
    force /= mass;
    acceleration += force;
}
*/
