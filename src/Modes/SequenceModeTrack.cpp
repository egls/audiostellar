#include "SequenceModeTrack.h"
#include "SequenceMode.h"

Sounds * SequenceModeTrack::sounds = NULL;
int SequenceModeTrack::intUnits[QTY_UNITS] = {QTY_STEPS/4, QTY_STEPS /2, QTY_STEPS, QTY_STEPS*2, QTY_STEPS*3, QTY_STEPS*4};


//Esto se llama una vez por semiCorchea
void SequenceModeTrack::onTempo() {
    if ( secuencia.size() == 0 ) {
        return;
    }

    if ( currentStep == -1 ) {
        currentStep = SequenceMode::currentBeat;
    } else {
        currentStep++;
        if ( currentStep >= getCantSteps() ) {
            currentStep = 0;
        }
    }

    if ( getCantSteps() >= QTY_STEPS ) {
        if ( currentStep % QTY_STEPS != SequenceMode::currentBeat ) {
//            ofLog() << "track unsynced!!";
            currentStep = SequenceMode::currentBeat;
        }
    } else {
        if ( SequenceMode::currentBeat % getCantSteps() != currentStep ) {
//            ofLog() << "track unsynced!!";
            currentStep = SequenceMode::currentBeat % getCantSteps();
        }
    }

//    ofLog() << "Beat: " << SequenceMode::currentBeat << " "
//            << "Step: " << currentStep << " "
//            << "Mod: " << currentStep % QTY_STEPS;

    //esto es que se resincronizo, ej. alguien le dio play desde otro programa
    //acá sucede que si alguien le da play justo en el último step, la secuencia no se reiniciará (pero estará a tempo)
    if ( SequenceMode::currentBeat == 0 && lastStep != (QTY_STEPS-1) ) {
        currentStep = 0;
    }

    if ( playing && secuenciaEnTiempo[currentStep] != NULL ) {
        if ( ofRandom(0,1) < probability && volume > 0  ) {
            sounds->playSound( secuenciaEnTiempo[currentStep], volume );
        }
    }
    lastStep = SequenceMode::currentBeat;
}

void SequenceModeTrack::toggleSound(Sound * sound, bool doProcessSequence) {
    if ( sound != NULL ) {
        if ( !sound->selected ) {
            //TODO : infinite sequences
            if ( secuencia.size() < getCantSteps() ) {
                secuencia.push_back( sound );
                sound->selected = true;
            }
        } else {
            secuencia.erase(
                std::remove(secuencia.begin(),
                secuencia.end(), sound), secuencia.end());

            sound->selected = false;
        }

        if ( doProcessSequence ) {
            processSequence();
        }
    }
}

SequenceModeTrack::SequenceModeTrack() {
    secuenciaEnTiempo = new Sound*[getCantSteps()];

    for ( int i = 0 ; i < getCantSteps() ; i++ ) {
        secuenciaEnTiempo[i] = NULL;
    }

    reset();

    offset.addListener(this, &SequenceModeTrack::onNeedToProcess);
    selectedUnit.addListener(this, &SequenceModeTrack::onNeedToProcess);
}

void SequenceModeTrack::selectAllSounds() {
    for ( unsigned int i = 0 ; i < secuencia.size() ; i++ ) {
        secuencia[i]->selected = true;
    }
}

int SequenceModeTrack::getCantSteps()
{
    return intUnits[selectedUnit];
}
void SequenceModeTrack::unselectAllSounds() {
    for ( unsigned int i = 0 ; i < secuencia.size() ; i++ ) {
        secuencia[i]->selected = false;
    }
}

void SequenceModeTrack::onNeedToProcess(int &a)
{
    processSequence();
}

void SequenceModeTrack::draw( bool notSelected ) {
    if ( isProcessing || currentStep == -1 ) {
        return;
    }

    ofSetLineWidth(1);

    if ( !notSelected && SequenceMode::isModeActive ) {
        ofSetColor(ofColor(255,255,255));
    } else {
        ofSetColor(ofColor(80,80,80));
    }

    polyLine.draw();

    if ( playing ) {
        ofSetColor(ofColor(255,255,255));
        ofFill();

        if ( secuencia.size() > 0 ) {
            ofPoint p;

            if ( secuenciaEnTiempo[currentStep] != NULL ) {
//                p = secuenciaEnTiempo[currentStep]->getPosition();
            } else {
                float offsetedStep = ( currentStep + ( getCantSteps() - offset ) ) % getCantSteps();
                p = polyLine.getPointAtPercent( offsetedStep / getCantSteps() );
            }
            if ( !(p.x == 0.0f && p.y == 0.0f)) {
                ofDrawEllipse( p.x , p.y, 5, 5 );
            }
        }
    }
}

void SequenceModeTrack::processSequence() {
    isProcessing = true;
    polyLine.clear();

    secuenciaEnTiempo = new Sound*[getCantSteps()];

    for ( int i = 0 ; i < getCantSteps() ; i++ ) {
        secuenciaEnTiempo[i] = NULL;
    }

    if ( secuencia.size() == 0 ) {
        isProcessing = false;
        return;
    }

    unsigned int secuenciaSize = secuencia.size();
    if ( getCantSteps() < secuencia.size() ) {
        secuenciaSize = getCantSteps();
    }

    // saco distancia total
    vector<float> distancias;
    float distanciaAcumulada = 0;
    for ( unsigned int i = 1 ; i < secuenciaSize ; i++ ) {
        float distancia = Utils::distance( secuencia[i-1]->getPosition(), secuencia[i]->getPosition() );
        distancias.push_back(distancia);
        distanciaAcumulada += distancia;
    }

    // if ( secuencia.size() == 2 ) {
        float distanciaUltimoConPrimero = Utils::distance( secuencia[secuencia.size()-1]->getPosition(), secuencia[0]->getPosition() );
        distanciaAcumulada += distanciaUltimoConPrimero;
    // }

//     cout << "Distancias iniciales: ";
//     for ( unsigned int i = 0 ; i < distancias.size() ; i++ ) {
//         cout << distancias[i] << ",";
//     }
//     cout << endl;

    secuenciaEnTiempo[0] = secuencia[0];

    int stepsAcumulados = 0;

    for ( unsigned int i = 0 ; i < distancias.size() ; i++ ) {
        distancias[i] = distancias[i] / distanciaAcumulada;
        int relacionDistanciaSteps = floor(distancias[i] * getCantSteps()); //aca es en relacion con cantSteps no QTY_STEPS

        if (relacionDistanciaSteps == 0) {
            relacionDistanciaSteps = 1;
        }

        distancias[i] = relacionDistanciaSteps + stepsAcumulados;

        stepsAcumulados += relacionDistanciaSteps;
    }

//     cout << "Distancias finales: ";
    for ( unsigned int i = 0 ; i < distancias.size() ; i++ ) {
//         cout << distancias[i] << ",";
        int step = distancias[i];
        secuenciaEnTiempo[ step ] = secuencia[i+1];
    }
//     cout << endl;

//    cout << "secuenciaEnTiempo: [";
//    for ( unsigned int i = 0 ; i < cantSteps ; i++ ) {
//        if ( secuenciaEnTiempo[i] == NULL ) {
//            cout << "0 ";
//        } else {
//            cout << "1 ";
//        }

//    }
//    cout << "]" << endl;

    for ( unsigned int i = 0 ; i < secuenciaSize ; i++ ) {
        polyLine.addVertex( secuencia[i]->getPosition().x,
                            secuencia[i]->getPosition().y, 0 );
    }

    polyLine.close();

    processSequenceOffset();

    isProcessing = false;
}

void SequenceModeTrack::clearSequence()
{
    unselectAllSounds();
    secuencia.clear();

    for ( int i = 0 ; i < getCantSteps() ; i++ ) {
        secuenciaEnTiempo[i] = NULL;
    }

    polyLine.clear();
    reset();
}

void SequenceModeTrack::processSequenceOffset()
{
    Sound ** secuenciaEnTiempoOffset = new Sound*[getCantSteps()];

    for ( int i = 0 ; i < getCantSteps() ; i++ ) {
        secuenciaEnTiempoOffset[ (i + offset) % getCantSteps() ] = secuenciaEnTiempo[i];
    }

    secuenciaEnTiempo = secuenciaEnTiempoOffset;

//    cout << "secuenciaEnTiempoOffset: [";
//    for ( unsigned int i = 0 ; i < cantSteps ; i++ ) {
//        if ( secuenciaEnTiempoOffset[i] == NULL ) {
//            cout << "0 ";
//        } else {
//            cout << "1 ";
//        }

//    }
    //    cout << "]" << endl;
}

void SequenceModeTrack::reset()
{
    probability = 1;
    offset = 0;
    volume = 1.0f;
    selectedUnit = 2;
}
