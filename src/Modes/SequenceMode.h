#pragma once

#define DEFAULT_TEMPO 120

#define QTY_TRACKS 32

#include "ofMain.h"
#include "Mode.h"
#include "SequenceModeTrack.h"
#include "Sounds.h"
#include "ofxSimpleTimer.h"
#include "Utils.h"
#include "ofxJSON.h"
#include "MidiServer.h"
#include "OscServer.h"
#include "Utils/UI.h"

class MidiServer;

class SequenceMode : virtual public Mode {
private:
    Sounds *sounds;
    MidiServer *midiServer;
    const int SLIDER_VOLUME_HEIGHT = 90;

    SequenceModeTrack tracks[QTY_TRACKS];
    SequenceModeTrack * selectedTrack;

    const vector<string> strUnits {"1/4 bar", "1/2 bar","1 bar", "2 bars", "3 bars", "4 bars"};

    int bpm = 120;
    bool useMidiClock = false;
    ofxSimpleTimer tempo;

    void onTempo(int &args);
public:
    SequenceMode(Sounds *_sounds, MidiServer *_midiServer);

    static int currentBeat;
    static bool isModeActive;

    //void setupGui();
    Json::Value save();
    void load( Json::Value jsonData );

    void mousePressed(ofVec2f p, int button);
    void mouseDragged(ofVec2f p, int button);
    void midiMessage(Utils::midiMsg m);
    void oscDispatcher(ofxOscMessage &m);

    void selectTrack( SequenceModeTrack * track );

    void reset();
    void update();
    void beforeDraw();
    void draw();
    void drawGui();

    void processSequence();

    void onSelectedMode();
    void onUnselectedMode();
};
