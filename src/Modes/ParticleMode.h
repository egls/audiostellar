#pragma once

#include "ofMain.h"
#include "Mode.h"
#include "Sounds.h"
#include "ParticleRegions.h"
#include "Particles/Particle.h"
#include "Particles/ParticleModels.h"
#include "ofxJSON.h"
#include "MidiServer.h"
#include "ofxImGui.h"

class MidiServer;

class ParticleMode: virtual public Mode {
private:
    vector<Particle*> particles;
    //vector<AttractorParticle*> attractors;
    vector<string> modelNames;
    vector<Tooltip::tooltip> modeTooltips;

    int atModel;
    float volume = 1.0;

    Sounds *sounds = NULL;
    MidiServer *midiServer = NULL;

    //Dibujo de áreas
    ParticleRegionsManager *drawer = NULL;

public:
    ParticleMode(Sounds *_sounds , MidiServer *_midiServer);

    vector<Particle*> add(ofVec2f p);
    //void addAttractor(ofVec2f p);
    void update();
    void reset();
    void draw();
    void drawGui();
    void beforeDraw();


    void mousePressed(ofVec2f p, int button);
    void keyPressed(int key);
    void mouseDragged(ofVec2f p, int button);
    void mouseReleased(ofVec2f p);
    void midiMessage(Utils::midiMsg m);
    bool midiLearn = false;

    //Sesión
    Json::Value save();
    void load( Json::Value jsonData );

    float age;
    bool randomizeEmitter;

    void deleteLastParticleRegion();
    
    //OSC
    const string OSC_EMIT = "/modes/particle/emit";
    const string OSC_EMIT_X = "/modes/particle/emit_x";
    const string OSC_EMIT_Y = "/modes/particle/emit_y";
    const string OSC_PARTICLE_VOL = "/modes/particle/vol";
    const string OSC_PARTICLE_LIFESPAN = "/modes/particle/lifespan";


    
    
    ofVec2f oscPosition;
    void oscDispatcher(ofxOscMessage &m);
    void oscPlay(ofVec2f position);



};
