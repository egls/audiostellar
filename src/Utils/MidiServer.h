#pragma once
\
#include "ofMain.h"
#include "ofxImGui.h"
#include "ofxMidi.h"
#include "ofxMidiClock.h"
#include "Modes/Modes.h"
#include "Utils.h"
#include "ColorPaletteGenerator.h"
#include "ofxSimpleTimer.h"
#include "ofxJSON.h"

//Esto es feo, pero soluciona un temita del orden de includes
class Modes;

struct midiCCMapping {
   string label;
   int cc;
   float *float_ptr;
   int *int_ptr;
   float min;
   float max;
};

class MidiServer:public ofxMidiListener {
private:

    /*
    ofxDatGui *gui = NULL;
    ofxDatGuiDropdown *midiDeviceSelector = NULL;
    ofxDatGuiToggle * btnMidiLearn = NULL;
    */


    Modes *modes = NULL;

    ofxMidiIn midiIn;
    ofxMidiClock clock; //< clock message parser

    vector<string> midiDevices;
    string currentMidiDevice = "Not set";

    int currentBeat = -1;
    int currentSyncBeat = -1;

    void checkForNewMidiDevices();
    void selectMidiDevice(int idx);
    void newMidiMessage(ofxMidiMessage& eventArgs);


    //MIDI CC
    midiCCMapping lastControlMoved;
    vector<midiCCMapping> ccMappings;

    //midiCCMapping defaultlastControlMoved = {.label= "", .cc = -1, .float_ptr = NULL, .int_ptr = NULL, .min = -1.0f, .max = -1.0f };
	midiCCMapping defaultlastControlMoved;
public:
    MidiServer();
    void init(Modes *_modes);
    void drawGui();
    ~MidiServer() {}

    static bool midiLearn;

    //Mappeable gui methods
    bool SliderFloat(string label, float *ptr, float min, float max);
    bool VSliderFloat(string label, const ImVec2 &size, float *ptr, float min, float max, string format);
    bool SliderInt(string label, int *ptr, int min, int max);

    Json::Value save();
    void load(Json::Value jsonData);
    void reset();

    //    bool clockRunning = false; //< is the clock sync running?
    //    unsigned int beats = 0; //< song pos in beats
    //    double seconds = 0; //< song pos in seconds, computed from beats
    //    double bpm = 120; //< song tempo in bpm, computed from clock length
    //
    string getCurrentMidiDevice();
    void setCurrentMidiDevice(string device);

    template<typename F>
    F trunc_decs(const F& f,
                     int decs);

};
