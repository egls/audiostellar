#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxImGui.h"
#include "Tooltip.h"
#include "tidalOscListener.h"

#define OSC_LOG true


class OscServer {
    
private:
    
    static int receivePort;
    static char receiveHost[64];
    static int sendPort;
    static char sendHost[64];
    
    static char hostname[128];

    OscServer();
    static OscServer* instance;
public:
    static OscServer* getInstance();

    
    ofxOscReceiver oscReceiver;
    static ofxOscSender oscSender;
    
    static ofEvent<ofxOscMessage> oscEvent;
    static bool enable;
    
    void update();
    
    void start();
    void stop();
    
    void drawGui();
    void getInterfaceIP();

    void static sendMessage(ofxOscMessage message);
    void static sendMessage(string address, float value);
    void static sendMessage(string address, int value);
    void static sendMessage(string address, string value);

    string static getSendHost();
    int static getReceivePort();
    int static getSendPort();

    void static setSendHost( string v );
    void static setReceivePort( int v );
    void static setSendPort( int v );


};





