#include "tidalOscListener.h"

TidalOscListener* TidalOscListener::instance  = nullptr;

TidalOscListener::TidalOscListener(){
    ofAddListener(OscServer::oscEvent, this, &TidalOscListener::onOscMsg);
    sounds = Sounds::getInstance();
}

TidalOscListener* TidalOscListener::getInstance(){
    if( instance == nullptr){
        instance = new TidalOscListener();
    }

    return instance;
}

bool TidalOscListener::isTidalMsg(ofxOscMessage &m){
    return m.getAddress() == TIDAL_MSG_HEADER;
}

void TidalOscListener::onOscMsg(ofxOscMessage &m){

    if(isTidalMsg(m)){
          string name = "";
          //ofLogNotice("MSG", ofToString(m));
          int sndIdx = 0;
          float volume = 1.0f;

          for(unsigned int i = 0; i < m.getNumArgs() ; i++){
            string str = m.getArgAsString(i);
            //SOUND
            if (str == "sound" || str == "s"){
                name = m.getArgAsString(i + 1);
            }

            // INDEX / NOTE
            if(str == "n"){
                sndIdx = m.getArgAsInt(i + 1);
            }

            // GAIN
            if( str == "gain"){
              volume = m.getArgAsFloat(i + 1);
            }
          }

          vector<Sound*> snds  = Sounds::getInstance()->getSoundsByCluster(name);

          //Wrap around N
          sndIdx = sndIdx % (snds.size() - 1);

          if(snds.size() != 0){
                Sounds::getInstance()->playSound(snds[sndIdx], volume);
          }
    }
}
