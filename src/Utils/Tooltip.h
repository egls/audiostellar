#pragma once

#include "ofMain.h"
#include "ofxImGui.h"



class Tooltip {
    
public:
    
    struct tooltip {
        string title;
        string content;
    };
    
    static bool enabled;
    static bool itemHovered;
    static string tooltipContent;
    static string tooltipTitle;
    
    static void setTooltip(tooltip tooltip);
    static void drawGui();
    
    static tooltip MASTER_VOLUME;
    static tooltip SOUNDS_REPLAY;
    static tooltip SOUNDS_ORIGINAL_POSITIONS;
    static tooltip SOUNDS_PLAY_ON_HOVER;
    static tooltip CLUSTERS_EPS;
    static tooltip CLUSTERS_MIN_PTS;
    static tooltip CLUSTERS_EXPORT_FILES;
    static tooltip CLUSTERS_SET_NAME;
    static tooltip MODE_EXPLORER;
    static tooltip MODE_PARTICLE;
    static tooltip MODE_SEQUENCE;
    static tooltip PARTICLE_VOLUME;
    static tooltip PARTICLE_LIFESPAN;
    static tooltip PARTICLE_RANDOMIZE_EMITTER;
    static tooltip PARTICLE_SIMPLE;
    static tooltip PARTICLE_SIMPLE_X_ACCELERATION;
    static tooltip PARTICLE_SIMPLE_Y_ACCELERATION;
    static tooltip PARTICLE_SWARM;
    static tooltip PARTICLE_SWARM_X_ACCELERATION;
    static tooltip PARTICLE_SWARM_Y_ACCELERATION;
    static tooltip PARTICLE_SPREAD_AMOUNT;
    static tooltip PARTICLE_EXPLOSION;
    static tooltip PARTICLE_EXPLOSION_SPEED;
    static tooltip PARTICLE_EXPLOSION_DENSITY;
    static tooltip ATTRACTOR_GRAVITY;
    static tooltip ATTRACTOR_MASS;
    static tooltip SEQUENCE_BPM;
    static tooltip SEQUENCE_ACTIVE;
    static tooltip SEQUENCE_VOLUME;
    static tooltip SEQUENCE_OFFSET;
    static tooltip SEQUENCE_PROBABILITY;
    static tooltip SEQUENCE_BARS;
    static tooltip SEQUENCE_CLEAR;
    static tooltip MIDI_DEVICES;
    static tooltip MIDI_LEARN;
    static tooltip MIDI_CLOCK;  

    static tooltip DIMREDUCT_FEATURE_SETTINGS;
    static tooltip DIMREDUCT_AUDIO_FEATURES;
    static tooltip DIMREDUCT_METRIC;
    static tooltip DIMREDUCT_AUDIO_SAMPLE_RATE;
    static tooltip DIMREDUCT_AUDIO_LENGTH;
    static tooltip DIMREDUCT_STFT;
    static tooltip DIMREDUCT_STFT_WINDOW_SIZE;
    static tooltip DIMREDUCT_STFT_HOP_SIZE;
    static tooltip DIMREDUCT_PCA;
    static tooltip DIMREDUCT_SAVE_PCA_RESULTS;
    static tooltip DIMREDUCT_FORCE_FULL_PROCESS;
    static tooltip DIMREDUCT_TSNE_PERPLEXITY;
    static tooltip DIMREDUCT_TSNE_LEARNING_RATE;
    static tooltip DIMREDUCT_TSNE_ITERATIONS;
    static tooltip DIMREDUCT_UMAP_NEIGHBORS;
    static tooltip DIMREDUCT_UMAP_MIN_DISTANCE;
    
    static tooltip OSC_RECEIVE_PORT;
    static tooltip OSC_RECEIVE_ADDRESS;
    static tooltip OSC_SEND_PORT;
    static tooltip OSC_SEND_ADDRESS;
    
    static tooltip AUDIO_SETTINGS_MAX_VOICES;
    static tooltip AUDIO_SETTINGS_ACTIVE_CHANNELS;
    
    static tooltip CONTEXT_MENU_SOUND_OUTPUT_CHANNELS;
    static tooltip CONTEXT_MENU_CLUSTER_OUTPUT_CHANNELS;
    
};

