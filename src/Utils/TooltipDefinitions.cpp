#include "Tooltip.h"

Tooltip::tooltip Tooltip::Tooltip::MASTER_VOLUME = { "Volume", "Set the master volume. This fader will only affect the first two output channels of the selected audio device in Audio settings." };
Tooltip::tooltip Tooltip::SOUNDS_REPLAY = { "Replay Sound", "When active allows to retrigger sounds as they are clicked." };
Tooltip::tooltip Tooltip::SOUNDS_ORIGINAL_POSITIONS = { "Original Positions", "Revert sounds to their original positions." };

Tooltip::tooltip Tooltip::CLUSTERS_EPS = { "DBScan Eps", "DBScan Eps parameter specifies how close points should be to each other to be considered as part of a cluster." };
Tooltip::tooltip Tooltip::CLUSTERS_MIN_PTS = { "DBScan MinPts", "DBScan MinPts specifies the minimum number of points to form a dense region." };
Tooltip::tooltip Tooltip::CLUSTERS_EXPORT_FILES  = { "Export files", "Export sounds in each cluster to folders." };
Tooltip::tooltip Tooltip::CLUSTERS_SET_NAME = { "Set cluster name", "Set a name for this cluster. Holding 'c' on your keyboard will show all cluster names at once." };

Tooltip::tooltip Tooltip::MODE_EXPLORER = { "Explorer Mode", "Play sounds by cliking or dragging on the map. Right click on each sound to get options." };
Tooltip::tooltip Tooltip::MODE_PARTICLE = { "Particle Mode", "Click on the map to drop particles. Each time a particle collides with a sound it triggers playback. Choose different particle modes and adjust parameters in the Tools menu to set particle trajectories." };
Tooltip::tooltip Tooltip::MODE_SEQUENCE = { "Sequence Mode", "Select sounds in the map to play them sequentially, in a loop. Sounds are played in order, as they are added to the sequence. The time interval between sounds in the sequence is related to their distances in the map." };

Tooltip::tooltip Tooltip::PARTICLE_VOLUME = { "Volume", "Set the volume for sounds triggered by particles." };
Tooltip::tooltip Tooltip::PARTICLE_LIFESPAN = { "Lifespan", "Set the lifespan of each particle. A value of 10 makes the particle permanent." };
Tooltip::tooltip Tooltip::PARTICLE_RANDOMIZE_EMITTER = { "Randomize Emitter position", "Randomize the position of each new particle inside a particle region. Only useful if you created a particle region using MIDI Learn." };

Tooltip::tooltip Tooltip::PARTICLE_SIMPLE = { "Simple", "Simple particles move in straight line." };
Tooltip::tooltip Tooltip::PARTICLE_SIMPLE_X_ACCELERATION = { "X", "Sets the acceleration in the X axis. Negative values invert direction." };
Tooltip::tooltip Tooltip::PARTICLE_SIMPLE_Y_ACCELERATION = { "Y", "Sets the acceleration in the Y axis. Negative values invert direction." };
Tooltip::tooltip Tooltip::PARTICLE_SWARM = { "Swarm", "Swarm particles perform a two dimensional random walk." };
Tooltip::tooltip Tooltip::PARTICLE_SWARM_X_ACCELERATION = { "X", "Sets the velocity in the X axis. Negative values invert direction." };
Tooltip::tooltip Tooltip::PARTICLE_SWARM_Y_ACCELERATION = { "Y", "Sets the velocity in the Y axis. Negative values invert direction." };
Tooltip::tooltip Tooltip::PARTICLE_SPREAD_AMOUNT = { "Spread amount", "Sets how strong the particles shake." };

Tooltip::tooltip Tooltip::PARTICLE_EXPLOSION = { "Explosion", "Explosion particles move in stright line in all directions away from an emitter. Click on the map to create a particle emitter." };
Tooltip::tooltip Tooltip::PARTICLE_EXPLOSION_SPEED = { "Speed", "Set the speed for explosion particles." };
Tooltip::tooltip Tooltip::PARTICLE_EXPLOSION_DENSITY = { "Density", "Set the number of explosion particles created from each emitter." };

Tooltip::tooltip Tooltip::ATTRACTOR_GRAVITY = { "Gravity", "" };
Tooltip::tooltip Tooltip::ATTRACTOR_MASS = { "Mass", "" };

Tooltip::tooltip Tooltip::SEQUENCE_BPM = { "BPM", "Set the tempo in beats per minute." };

Tooltip::tooltip Tooltip::SEQUENCE_ACTIVE = { "Select sequence track", "Click to edit sequence parameters." };
Tooltip::tooltip Tooltip::SEQUENCE_VOLUME = { "Sequence Volume", "Set the volume for each sequence track." };
Tooltip::tooltip Tooltip::SEQUENCE_OFFSET = { "Offset", "Set the offset time from the starting beat in sixteenth notes resolution." };
Tooltip::tooltip Tooltip::SEQUENCE_PROBABILITY = { "Probability", "Set the probability to be played for all sounds in the sequence." };
Tooltip::tooltip Tooltip::SEQUENCE_BARS = { "Bars", "Set the duration of the sequence." };
Tooltip::tooltip Tooltip::SEQUENCE_CLEAR = { "Clear sequence", "Delete all the events in the sequence." };

Tooltip::tooltip Tooltip::MIDI_DEVICES = { "Midi Devices", "Select available Midi devices." };
Tooltip::tooltip Tooltip::MIDI_LEARN = { "Midi Learn", "In Explorer Mode map a sound to a MIDI note. In Particle Mode, click and drag to draw a particle region and map a MIDI note to it. In any mode, moving any slider in AudioStellar and then moving a fader in your controller will map them together." };
Tooltip::tooltip Tooltip::MIDI_CLOCK = { "Use MIDI clock", "Get the tempo from an external MIDI device." };

Tooltip::tooltip Tooltip::DIMREDUCT_AUDIO_SAMPLE_RATE = { "Sample rate", "Your audio samples will be converted to this sample rate for processing. Original audios will not be affected." };
Tooltip::tooltip Tooltip::DIMREDUCT_AUDIO_LENGTH = { "Audio length", "Set the length of the audio samples in seconds. If your files are longer they will be chopped otherwise they will be zero padded to the specified duration. Only for processing, original audios will not be affected." };
Tooltip::tooltip Tooltip::DIMREDUCT_AUDIO_FEATURES = { "Audio feature extraction", "Set features to extract from audio." };
Tooltip::tooltip Tooltip::DIMREDUCT_FEATURE_SETTINGS = { "Feature settings", "Select the features you would like to process. This parameters will affect feature extraction and not your original audios or how they will sound in the application." };
Tooltip::tooltip Tooltip::DIMREDUCT_METRIC = { "Metric", "Similarity measure you would like to use for the visualization." };
Tooltip::tooltip Tooltip::DIMREDUCT_STFT = { "Short Time Fourier Transform", "" };
Tooltip::tooltip Tooltip::DIMREDUCT_STFT_WINDOW_SIZE = { "Window size", "Set the size of the analysis in number of samples. Smaller windows produce more precise time resolution while larger windows increase frequency resolution." };
Tooltip::tooltip Tooltip::DIMREDUCT_STFT_HOP_SIZE = { "Hop size", "Set the offset for the overlapping windows." };
Tooltip::tooltip Tooltip::DIMREDUCT_PCA = { "Principal Component Analysis", "PCA performs a linear mapping of data to a lower-dimensional space in such a way that the variance of the data in the low dimensional representation is maximized." };
Tooltip::tooltip Tooltip::DIMREDUCT_UMAP_NEIGHBORS = { "UMAP N Neighbors", "Controls how UMAP balances local versus global structure in the data. Low values will force UMAP to concentrate on very local structure (potentially to the detriment of the big picture), while large values will push UMAP to look at larger neighborhoods." };
Tooltip::tooltip Tooltip::DIMREDUCT_UMAP_MIN_DISTANCE = { "UMAP Minimum distance", "Controls how tightly UMAP is allowed to pack points together. Low values will result in clumpier embeddings." };
Tooltip::tooltip Tooltip::DIMREDUCT_TSNE_PERPLEXITY = { "Perplexity", "Set the number of effective nearest neighbors. Larger dataset requires a larger perplexity." };
Tooltip::tooltip Tooltip::DIMREDUCT_TSNE_ITERATIONS = { "t-SNE's iteration count.", "Set Maximum number of iterations for the optimization. Should be at least 250." };
Tooltip::tooltip Tooltip::DIMREDUCT_TSNE_LEARNING_RATE = { "t-SNE's learning rate.", "Set t-SNE's learning rate. Usually in the range 10.0, 1000.0." };
Tooltip::tooltip Tooltip::DIMREDUCT_SAVE_PCA_RESULTS = { "Save intermediate results", "Save feature extraction and PCA results to disk for greatly reducing process time in future processes." };
Tooltip::tooltip Tooltip::DIMREDUCT_FORCE_FULL_PROCESS = { "Force full process", "Do full process even if intermediate results are present. Use this if you change or add new sounds to your folder." };

Tooltip::tooltip Tooltip::OSC_RECEIVE_PORT = { "Receive Port", "Set the port number for incoming OSC messages." };
Tooltip::tooltip Tooltip::OSC_RECEIVE_ADDRESS = { "Receive Address", "This is the IP address where OSC messages should be sent to AudioStellar from an application in another device. If you want to receive OSC from an application running in this computer set 127.0.0.1 as your host in the sending application." };
Tooltip::tooltip Tooltip::OSC_SEND_PORT = { "Send Port", "Set the port number for outgoing OSC messages." };
Tooltip::tooltip Tooltip::OSC_SEND_ADDRESS = { "Send Address", "Set the IP address of the device to receive OSC messages from AudioStellar. If you are sending OSC to an application running in this computer set this address to 127.0.0.1" };

Tooltip::tooltip Tooltip::AUDIO_SETTINGS_MAX_VOICES = { "Max Voices", "Set the maximum number of sounds to be played simultaneously."};
Tooltip::tooltip Tooltip::AUDIO_SETTINGS_ACTIVE_CHANNELS = { "Active Channels", "Click on a channel to activate or deactivate it. Only active channels will be available. You can select stereo pairs or individual mono outputs."};


Tooltip::tooltip Tooltip::CONTEXT_MENU_SOUND_OUTPUT_CHANNELS = { "Output Channels", "Select the output channel for individual sounds. Only the active outputs are available in this menu. To activate outputs select them in Audio settings menu."};
Tooltip::tooltip Tooltip::CONTEXT_MENU_CLUSTER_OUTPUT_CHANNELS = { "Output Channels", "Select the output channel for individual clusters. Only the active outputs are available in this menu. To activate outputs select them in Audio settings menu."};


