#pragma once
#include "OscServer.h"
#include "Sounds.h"


class Sounds;

class TidalOscListener{

private:
    TidalOscListener();
    static TidalOscListener* instance;
    const string TIDAL_MSG_HEADER = "/playTidal";
    const string SND_IDX_DELIMITER = ":";

    Sounds * sounds = NULL;

public:
    static TidalOscListener* getInstance();
    bool isTidalMsg(ofxOscMessage &m);

    void onOscMsg(ofxOscMessage &m);


};
