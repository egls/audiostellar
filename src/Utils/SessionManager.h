#pragma once

#include "ofMain.h"
#include "Utils.h"
#include "ofxJSON.h"
#include "Modes/Modes.h"
#include "Sound/Sounds.h"
#include "Utils/MidiServer.h"
#include "ofxImGui.h"
#include "OscServer.h"
#include "UI.h"
#include "AudioEngine.h"
#include "Voices.h"

class Modes;
class MidiServer;

class SessionManager {

private:

    Modes * modes = NULL;
    Sounds * sounds = NULL;
    MidiServer * midiServer = NULL;
    AudioEngine * audioEngine = NULL;
    Voices * voices = NULL;

    const string STRING_ERROR = "ERROR";

    //SESSION
    struct datasetResult{
        bool success;
        string status;
        ofxJSONElement data;
    };

    string jsonFilename = "";
    ofxJSONElement jsonFile;

    string userSelectJson();
    datasetResult validateDataset(string datasetPath);

    bool sessionLoaded = false;

    //STELLAR FILE
    string SETTINGS_FILENAME = "stellar.json"; //path will change depending on OS on constructor
    ofxJSONElement settingsFile;
    bool settingsFileExist();
    void createSettingsFile();
    void loadSettings();
    void saveSettingsFile();
   

    //RECENT PROJECTS
    ofFile recentProjectsFile;
    vector<string> recentProjects;
    vector<string> loadRecentProjects();
    void saveToRecentProject(string path);
    const string DEFAULT_SESSION = "assets/default_session/default.session.json";
    const string DEFAULT_SESSION_DIRECTORY = "assets/default_session/drumkits.mp3/";
    const string DEFAULT_SESSION_PLACEHOLDER = "DEFAULT_SESSION";

    //LAST MIDI DEV
    string lastSavedMidiDevice = "";

    void setWindowTitleWithSessionName();

public:

    SessionManager(Modes * modes,
                   MidiServer * midiServer);


    struct datasetLoadOptions{
        string method;
        string path;
    };

    void loadInitSession();
    void loadSession(datasetLoadOptions opts);
    void loadDefaultSession();
    void saveSession();
    bool isDefaultSession = false;

    void saveAsNewSession();
    void exit();

    bool areAnyRecentProjects(vector<string> recentProjects);

    //Getters
    bool getSessionLoaded();
    vector<string> getRecentProjects();

    void keyPressed(ofKeyEventArgs & e);
};
