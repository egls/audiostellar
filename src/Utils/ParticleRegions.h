#ifndef PARTICLEAREADRAWER
#define PARTICLEAREADRAWER

#include "ofMain.h"

class ParticleRegion{
private:

    float r;//temp for pretty drawing
    int stepSize;
    bool settedCenter;
    int prevY;
    int actualY;


public:
    ParticleRegion();
    void draw();
    void update();
    void mouseDragged(ofVec2f p);
    void mouseReleased();
    ofVec2f getRandomPointInside();
    bool isParticleInsideRegion(ofVec2f p);

    bool assigned;
    int note;
    float radius;
    ofVec2f centerPoint;

};

class ParticleRegionsManager{
private:
    ParticleRegion* area = nullptr;

public:
    ParticleRegionsManager();
    void draw();
    void update();
    void destroy();
    void destroyLastArea();
    void destroyLastAssignedArea();
    void mouseReleased();
    void mouseDragged(ofVec2f p);
    void onNoteOn(int note);
    ParticleRegion * getLastArea();
    ParticleRegion * getLastAssignedArea();

    vector<ParticleRegion *> areas;
    string status;

};
#endif
