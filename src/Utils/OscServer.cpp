#include "OscServer.h"

ofEvent<ofxOscMessage> OscServer::oscEvent = ofEvent<ofxOscMessage>();
bool OscServer::enable = false;
ofxOscSender OscServer::oscSender;

int OscServer::receivePort = 8000;
char OscServer::receiveHost[64] = "127.0.0.1";
int OscServer::sendPort = 9000;
char OscServer::sendHost[64] = "127.0.0.1";
char OscServer::hostname[128];

OscServer* OscServer::instance = nullptr;

OscServer::OscServer() {
    getInterfaceIP();
    //Básicamente alguien tiene que llamar a este método una vez para que se instancie...
    TidalOscListener::getInstance();
}

OscServer *OscServer::getInstance()
{
    if ( instance == nullptr ) {
        instance = new OscServer();
    }
    return instance;
}


void OscServer::update() {
    if (enable) {
        if (oscReceiver.hasWaitingMessages()) {
            ofxOscMessage m;
            while(oscReceiver.hasWaitingMessages()){
                oscReceiver.getNextMessage(m);
                /*
                ofLog() << "OSC: " << m.getAddress();
                ofLogNotice("MSG: " , ofToString(m.getNumArgs()));
                for(unsigned int i = 0; i < m.getNumArgs() ; i ++){
                  string str = m.getArgAsString(i);
                  ofLogNotice("VAL " + ofToString(i), str );
                }
                */
                ofNotifyEvent(oscEvent, m, this);
            }
        }
    }
}

void OscServer::start() {

    oscReceiver.setup(receivePort);
    oscSender.setup(sendHost, sendPort);
    enable = true;
}

void OscServer::stop() {

    oscReceiver.stop();
    oscSender.clear();
    enable = false;
}

void OscServer::drawGui() {
    
    if(ImGui::Checkbox("Enable", &enable)) enable ? start() : stop();
    
    ImGui::Text("");
    
    ImGui::InputInt("Receive port", &receivePort, NULL, NULL);
    if(ImGui::IsItemDeactivatedAfterEdit()) oscReceiver.setup(receivePort);
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::OSC_RECEIVE_PORT);
    
    ImGui::InputText("Receive address", receiveHost, 64, ImGuiInputTextFlags_ReadOnly);
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::OSC_RECEIVE_ADDRESS);
    
    ImGui::Text("");
    
    ImGui::InputInt("Send port", &sendPort, NULL, NULL);
    if(ImGui::IsItemDeactivatedAfterEdit()) oscSender.setup(sendHost, sendPort);
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::OSC_SEND_PORT);
    
    ImGui::InputText("Send address", sendHost, 64);
    if(ImGui::IsItemDeactivatedAfterEdit()) oscSender.setup(sendHost, sendPort);
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::OSC_SEND_ADDRESS);

}

void OscServer::getInterfaceIP() {
//    string address;
//    Poco::Net::NetworkInterface::List interfaces;
    
//    interfaces = ofxNet::NetworkUtils::listNetworkInterfaces(ofxNet::NetworkUtils::SITE_LOCAL);
    
//    for (const auto& interface: interfaces) {
//        address = interface.address().toString();
//    }
    
//    strcpy(receiveHost, address.c_str());
    vector<string> result;

    #ifdef TARGET_WIN32

        string commandResult = ofSystem("ipconfig");
        //ofLogVerbose() << commandResult;

        for (int pos = 0; pos >= 0; )
        {
            pos = commandResult.find("IPv4", pos);

            if (pos >= 0)
            {
                pos = commandResult.find(":", pos) + 2;
                int pos2 = commandResult.find("\n", pos);

                string ip = commandResult.substr(pos, pos2 - pos);

                pos = pos2;

                if (ip.substr(0, 3) != "127") // let's skip loopback addresses
                {
                    result.push_back(ip);
                    //ofLogVerbose() << ip;
                }
            }
        }

    #endif
    #ifdef TARGET_LINUX
        string commandResult = ofSystem("ip addr");

        for(int pos = 0; pos >= 0; )
        {
           pos = commandResult.find("inet ", pos);

            if(pos >= 0)
            {
                int pos2 = commandResult.find("brd", pos);

                string ip = commandResult.substr(pos+5, pos2-pos-9);

                pos = pos2;

                if(ip.substr(0, 3) != "127") // let's skip loopback addresses
                {
                    result.push_back(ip);
                    //ofLogVerbose() << ip;
                }
            }
        }

    #endif
    #ifdef TARGET_OSX
        string commandResult = ofSystem("ifconfig");

        for(int pos = 0; pos >= 0; )
        {
           pos = commandResult.find("inet ", pos);

            if(pos >= 0)
            {
                int pos2 = commandResult.find("netmask", pos);

                string ip = commandResult.substr(pos+5, pos2-pos-6);

                pos = pos2;

                if(ip.substr(0, 3) != "127") // let's skip loopback addresses
                {
                    result.push_back(ip);
                    //ofLogVerbose() << ip;
                }
            }
        }

    #endif

    if ( result.size() > 0 ) {
        strcpy(receiveHost, result[0].c_str());
    }
}

void OscServer::sendMessage(ofxOscMessage message)
{
    if ( enable ) {
        #if OSC_LOG
        string arguments = "";
        for ( unsigned int i = 0 ; i < message.getNumArgs() ; i++ ) {
            arguments += message.getArgAsString(i) + " ";
        }
        ofLog() << "Sending message: " << message.getAddress() << " " << arguments;
        #endif
        oscSender.sendMessage(message);
    }
}
void OscServer::sendMessage(string address, float value)
{
    ofxOscMessage message;
    message.setAddress(address);
    message.addFloatArg(value);
    sendMessage(message);
}
void OscServer::sendMessage(string address, int value)
{
    ofxOscMessage message;
    message.setAddress(address);
    message.addIntArg(value);
    sendMessage(message);
}
void OscServer::sendMessage(string address, string value)
{
    ofxOscMessage message;
    message.setAddress(address);
    message.addStringArg(value);
    sendMessage(message);
}


string OscServer::getSendHost()
{
    return sendHost;
}

int OscServer::getReceivePort()
{
    return receivePort;
}

int OscServer::getSendPort()
{
    return sendPort;
}

void OscServer::setSendHost(string v)
{
    strcpy(sendHost, v.c_str());
}

void OscServer::setReceivePort(int v)
{
    receivePort = v;
}

void OscServer::setSendPort(int v)
{
    sendPort = v;
}
