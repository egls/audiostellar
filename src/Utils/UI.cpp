#include "UI.h"

Gui* Gui::instance = nullptr;

Gui::Gui(MidiServer * midiServer,
         Modes * modes,
         SessionManager * sessionManager,
         OscServer *oscServer) {

    this->sounds = Sounds::getInstance();
    this->midiServer = midiServer;
    this->oscServer = oscServer;
    this->modes = modes;
    this->sessionManager = sessionManager;
    this->tooltip = new Tooltip();
    this->audioEngine = AudioEngine::getInstance();
    this->player = Voices::getInstance();

    font.load( OF_TTF_MONO, 10 );

    gui.setup(new GuiTheme(), true);
    drawGui = true;
}

Gui* Gui::getInstance(){
    /* Esto puede fallar??
    if(instance == 0){
        instance = new Gui();
    }
    */
    return instance;
}

Gui* Gui::getInstance(MidiServer * midiServer,
                      Modes * modes,
                      SessionManager * sessionManager,
                      OscServer * oscServer)
{
    if(instance == nullptr){
        instance = new Gui(midiServer, modes, sessionManager, oscServer);
    }
    return instance;
}


void Gui::newSession()
{
    haveToDrawDimReductScreen = true;
    isDimReductScreenOpen = true;
}

void Gui::drawProcessingFilesWindow()
{
    if ( isProcessingFiles ) {
        adr.checkProcess();

        if ( adr.progress == 1.0f ) {
            isProcessingFiles = false;

            string resultJSONpath = AudioDimensionalityReduction::generatedJSONPath;
            if ( resultJSONpath != "" ) {
                //sessionManager->loadSession(resultJSONpath.c_str());
                SessionManager::datasetLoadOptions opts;
                opts.method = "RECENT";
                opts.path = resultJSONpath.c_str();
                sessionManager->loadSession(opts);

                string strSuccess = "Hooray! Your map is done. \n\nNow adjust clustering settings.\n";

                if ( !adr.failFiles.empty() ) {
                    strSuccess += "\n:( Some files couldn't be processed: ";
                    for ( string s : adr.failFiles ) {
                        strSuccess += "\n" + s;
                    }
                }

                showMessage( strSuccess, "Process ended successfully" );
                sounds->setClusteringOptionsScreenFlags();
            } else {
                showMessage( "Unexpected error", "Error" );
            }
            AudioDimensionalityReduction::end();
        } else if ( adr.progress == -1.0f ) {
            isProcessingFiles = false;
            showMessage( adr.currentProcess, "Error" );
            AudioDimensionalityReduction::end();
        } else {
            ImGuiWindowFlags window_flags = 0;
            window_flags |= ImGuiWindowFlags_NoScrollbar;
            window_flags |= ImGuiWindowFlags_NoMove;
            window_flags |= ImGuiWindowFlags_NoResize;
            window_flags |= ImGuiWindowFlags_NoCollapse;
            window_flags |= ImGuiWindowFlags_NoNav;
            window_flags |= ImGuiWindowFlags_AlwaysAutoResize;
            ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - 200 ,100));

            bool isWindowOpen = true;

            string windowTitle = "Processing " +
                    ofToString(adr.filesFound) +
                    " files...";

            if ( adr.filesFound == 0 ) {
                windowTitle = "Processing files...";
            }

            if ( ImGui::Begin( windowTitle.c_str(), &isWindowOpen, window_flags)) {

                isProcessingFilesWindowHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem | ImGuiHoveredFlags_AllowWhenBlockedByPopup | ImGuiHoveredFlags_ChildWindows);

                ImGui::Text("Grab a coffee, "
                            "this can take quite a while. ");

                #ifndef TARGET_OS_WINDOWS
                ImGui::ProgressBar( adr.progress );
                #else
                ImGui::Text( ofToString(adr.progress).c_str() );
                #endif

                ImGui::Text( adr.currentProcess.c_str() );

                ImGui::TextDisabled("\n\n(monitoring your RAM usage is also a good idea)");
            }
            ImGui::End();
        }
    }
}

void Gui::draw() {

    gui.begin();

    drawMainMenu();

    if(drawGui) {
        modes->drawModeSelector();

        //Tools window
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoMove;
        window_flags |= ImGuiWindowFlags_NoResize;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoNav;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

        auto mainSettings = ofxImGui::Settings();
        mainSettings.windowPos = ImVec2(ofGetWidth() - WINDOW_TOOLS_WIDTH - 20, 30);
        mainSettings.lockPosition = true;

        if ( isToolsWindowHovered )
            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, 1.f);
        else
            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, .7f);
        if( ofxImGui::BeginWindow("Tools", mainSettings, window_flags)){
                ImGui::SetWindowSize( ImVec2(WINDOW_TOOLS_WIDTH,WINDOW_TOOLS_HEIGHT) );
            isToolsWindowHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem
                                                          | ImGuiHoveredFlags_AllowWhenBlockedByPopup
                                                          | ImGuiHoveredFlags_ChildWindows);

                ofxImGui::AddParameter(Voices::getInstance()->masterVolume);
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::MASTER_VOLUME);

                if ( ImGui::Checkbox("Midi Learn", &midiServer->midiLearn) ) {
                    if (midiServer->getCurrentMidiDevice() == "Not set") {
                        midiServer->midiLearn = false;
                        Gui::getInstance()->showMessage(
                                                        "No MIDI device selected"
                                                        ".\n\n"
                                                        "Please make sure your device is connected and select it in Settings menu.",
                                                        "Warning" );
                    }
                }
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::MIDI_LEARN);

                modes->drawModesSettings();
        }

        ofxImGui::EndWindow(mainSettings);
        ImGui::PopStyleVar();
    }

    Tooltip::drawGui();

    drawProcessingFilesWindow();
    drawAboutScreen();
    drawDimReductScreen();
    drawTutorial();

    sounds->drawClusteringOptionsScreen();

    drawAudioSettingsScreen();
    drawOscSettingsScreen();
    drawContextMenu();

    haveToHideCursor ? ofHideCursor() : ofShowCursor();

    if(toggleFullscreen && !isFullscreen || !toggleFullscreen && isFullscreen){
        ofToggleFullscreen();
        isFullscreen = !isFullscreen;
    }

    drawWelcomeScreen();

    drawFPS();
    drawMessage();

    gui.end();
}

void Gui::drawWelcomeScreen(){

   if(isWelcomeScreenOpen){

       ImGuiWindowFlags window_flags = 0;
       int w = 400;
       window_flags |= ImGuiWindowFlags_NoScrollbar;
       window_flags |= ImGuiWindowFlags_NoMove;
       window_flags |= ImGuiWindowFlags_NoResize;
       window_flags |= ImGuiWindowFlags_NoCollapse;
       window_flags |= ImGuiWindowFlags_NoNav;
       window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

       ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - w/2, 100));

       ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding,ImVec2(20,20));

       if (ImGui::Begin("Welcome to AudioStellar" , &isWelcomeScreenOpen, window_flags)){

           isWelcomeScreenHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem | ImGuiHoveredFlags_AllowWhenBlockedByPopup | ImGuiHoveredFlags_ChildWindows);

           ImGui::PushTextWrapPos(w);
           ImGui::TextWrapped(welcomeScreenText.c_str());
           ImGui::Columns(2,NULL, false);

           if(ImGui::Button("Yes, please.")) {
              isWelcomeScreenOpen = false;
              isWelcomeScreenHovered = false;
              startTutorial();
           }

           ImGui::NextColumn();

           if(ImGui::Button("No, I can handle this.")){
               isWelcomeScreenOpen = false;
               isWelcomeScreenHovered = false;
           }

           ImGui::End();
       }

       ImGui::PopStyleVar();
   } else {
       isWelcomeScreenHovered = false;
   }

}

void Gui::drawMainMenu(){
    if(ImGui::BeginMainMenuBar()){

        isMainMenuHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem
                                                   | ImGuiHoveredFlags_AllowWhenBlockedByPopup
                                                   | ImGuiHoveredFlags_ChildWindows);

        if(ImGui::BeginMenu("File")){

            if ( ImGui::MenuItem( "New", TARGET_OS_MAC ? "Cmd+N" : "Ctrl+N") ) {
                newSession();
            }

            if(ImGui::MenuItem("Open", TARGET_OS_MAC ? "Cmd+O" : "Ctrl+O")){
                SessionManager::datasetLoadOptions opts;
                opts.method = "GUI";
                sessionManager->loadSession(opts);
            }

            if ( !sessionManager->isDefaultSession ) {
                if(ImGui::MenuItem("Save", TARGET_OS_MAC ? "Cmd+S" : "Ctrl+S")){
                    sessionManager->saveSession();
                }
            } else {
                ImGui::PushStyleColor(ImGuiCol_Text,ImVec4(0.5,0.5,0.5,1.0));
                if ( ImGui::MenuItem("Save") ) {
                    showMessage("Cannot save default project, use \"Save as\" instead.");
                }
                ImGui::PopStyleColor();
            }

            if(ImGui::MenuItem("Save as...", TARGET_OS_MAC ? "Cmd+Shift+S" : "Ctrl+Shift+S")){
//            if(ImGui::MenuItem("Save as...")){
                sessionManager->saveAsNewSession();
            }

            if(ImGui::BeginMenu("Recent Projects")){
                vector<string> recentProjects = sessionManager->getRecentProjects();
                if(sessionManager->areAnyRecentProjects(recentProjects)){
                    for(unsigned int i = 0; i < recentProjects.size(); i++){
                        if(ImGui::MenuItem(recentProjects[i].c_str())){
                            //sessionManager->loadSession(recentProjects[i].c_str());
                            SessionManager::datasetLoadOptions opts;
                            opts.method = "RECENT";
                            opts.path = recentProjects[i].c_str();
                            sessionManager->loadSession(opts);
                        }
                    }
                }

                if(ImGui::MenuItem("Default project")) {
                    sessionManager->loadDefaultSession();
                }

                ImGui::EndMenu();
            }

//            if(ImGui::MenuItem("Quit", TARGET_OS_MAC ? "Cmd+Q" : "Ctrl+Q")){
            if(ImGui::MenuItem("Quit")){
                exit(0);
            }

            ImGui::EndMenu();
        }

        if(ImGui::BeginMenu("View")){
            sounds->drawGui();
            ImGui::Checkbox("Show UI", &drawGui);
            ImGui::Checkbox("Show sound filenames", &sounds->showSoundFilenamesTooltip);
            ImGui::Checkbox("Show FPS", &haveToDrawFPS);
            ImGui::Checkbox("Hide Cursor", &haveToHideCursor);
            ImGui::Checkbox("Fullscreen", &toggleFullscreen);
            ImGui::Checkbox("Show Tooltips", &Tooltip::enabled);
            ImGui::EndMenu();
        }

        if(ImGui::BeginMenu("Settings")){

            if ( ImGui::MenuItem("Audio settings") ) {
                haveToDrawAudioSettingsScreen = true;
                isAudioSettingsScreenOpen = true;
            }
            ImGui::Separator();

            if ( ImGui::MenuItem("Clustering settings") ) {
                sounds->setClusteringOptionsScreenFlags();
            }
            ImGui::Separator();

            midiServer->drawGui();

            if ( ImGui::MenuItem("OSC settings") ) {
                haveToDrawOscSettingsScreen = true;
                isOscSettingsScreenOpen = true;
            }

            ImGui::EndMenu();
        }

        if(ImGui::BeginMenu("Help")){
            if(ImGui::MenuItem("About")){
                haveToDrawAboutScreen = true;
                isAboutScreenOpen = true;
            }
            if(ImGui::MenuItem("Tutorial")){
                startTutorial();
            }
            if(ImGui::MenuItem("Source code")){
                string URL = "https://gitlab.com/ayrsd/audiostellar/";
                string command = "";

                #ifdef TARGET_LINUX
                command = "xdg-open ";
                #endif
                #ifdef TARGET_OSX
                command = "open ";
                #endif
                #ifdef TARGET_WIN32
                command = "start ";
                #endif

                ofSystem(command + URL);
            }
            if(ImGui::MenuItem("OSC Documentation")){
                string URL = "https://gitlab.com/ayrsd/audiostellar/-/blob/master/OSC_Documentation.md";
                string command = "";

                #ifdef TARGET_LINUX
                command = "xdg-open ";
                #endif
                #ifdef TARGET_OSX
                command = "open ";
                #endif
                #ifdef TARGET_WIN32
                command = "start ";
                #endif

                ofSystem(command + URL);
            }
           ImGui::EndMenu();
        }
        ImGui::EndMainMenuBar();
    }
}

void Gui::drawAboutScreen(){

    if(haveToDrawAboutScreen) {
        if(isAboutScreenOpen){

            ImGuiWindowFlags window_flags = 0;
            window_flags |= ImGuiWindowFlags_NoScrollbar;
            window_flags |= ImGuiWindowFlags_NoMove;
            window_flags |= ImGuiWindowFlags_NoResize;
            window_flags |= ImGuiWindowFlags_NoCollapse;
            window_flags |= ImGuiWindowFlags_NoNav;
            window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

            ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - 200, ofGetHeight()/3));
            if(ImGui::Begin("About", &isAboutScreenOpen, window_flags)){

                isAboutScreenHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem | ImGuiHoveredFlags_AllowWhenBlockedByPopup | ImGuiHoveredFlags_ChildWindows);

                ImGui::Text("Thanks for using AudioStellar v0.10.0 \n\n\nMore info at http://www.audiostellar.xyz");
            }
            ImGui::End();
        }else{
            haveToDrawAboutScreen = false;
            isAboutScreenHovered = false;
        }
    }
}

void Gui::drawDimReductScreen(){

    if(haveToDrawDimReductScreen) {

        if(isDimReductScreenOpen){

            ImGuiWindowFlags window_flags = 0;
            int w = 400;
            window_flags |= ImGuiWindowFlags_NoScrollbar;
            window_flags |= ImGuiWindowFlags_NoMove;
            window_flags |= ImGuiWindowFlags_NoResize;
            window_flags |= ImGuiWindowFlags_NoCollapse;
            window_flags |= ImGuiWindowFlags_NoNav;
            window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

            ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - w/2, 100));
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding,ImVec2(20,20));

            if (ImGui::Begin("Generate new session" , &isDimReductScreenOpen, window_flags)){

                isDimReductScreenHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem | ImGuiHoveredFlags_AllowWhenBlockedByPopup | ImGuiHoveredFlags_ChildWindows);

                //path
                ImGui::Text("Folder location");

                string folder;
                string displayFolder;

                if(adr.dirPath.size()) {
                    folder = ofFilePath::getPathForDirectory(adr.dirPath);
                    displayFolder = folder;
                }

                ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0,3));
                ImGui::BeginChild("child", ImVec2(208, 19), true);
                ImGui::TextUnformatted( displayFolder.c_str() );
                ImGui::EndChild();
                ImGui::PopStyleVar();

                ImGui::SameLine();
                if(ImGui::Button("Select folder")){
                    ofFileDialogResult dialogResult = ofSystemLoadDialog("Select Folder", true, ofFilePath::getUserHomeDir());
                    string path;

                    if(dialogResult.bSuccess) {
                        adr.dirPath = dialogResult.getPath();

                    }
                }

                if(ImGui::TreeNode("Advanced settings")) {
                    if(ImGui::TreeNode("Feature settings")) {
                        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_FEATURE_SETTINGS);

                        ImGui::Checkbox("STFT", &adr.stft);
                        ImGui::Checkbox("MFCC", &adr.mfcc);
                        ImGui::Checkbox("Spectral-Centroid", &adr.spectralCentroid);
                        ImGui::Checkbox("Chromagram", &adr.chromagram);
                        ImGui::Checkbox("RMS", &adr.rms);

                        if ( !adr.stft && !adr.mfcc && !adr.spectralCentroid && !adr.chromagram && !adr.rms ) {
                            adr.stft = true;
                        }

                        ImGui::Text("");

                        ImGui::InputFloat("Audio length", &adr.targetDuration, 0.1f, 0.5f, "%.1f");
                        adr.targetDuration = ofClamp(adr.targetDuration,0,500);
                        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_AUDIO_LENGTH);

                        ImGui::Combo("Sample rate", &sample_rate_item_current, sample_rate_items, IM_ARRAYSIZE(sample_rate_items));
                        adr.targetSampleRate = sample_rate_values[sample_rate_item_current];
                        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_AUDIO_SAMPLE_RATE);

                        ImGui::Combo("Window size", &window_size_item_current, window_size_items, IM_ARRAYSIZE(window_size_items));
                        adr.stft_windowSize = window_size_values[window_size_item_current];
                        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_STFT_WINDOW_SIZE);

                        ImGui::Combo("Hop size", &hop_size_item_current, hop_size_items, IM_ARRAYSIZE(hop_size_items));
                        adr.stft_hopSize = window_size_values[window_size_item_current] / hop_size_values[hop_size_item_current];
                        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_STFT_HOP_SIZE);
                        ImGui::Text("");


                        ImGui::TreePop();
                    }

                    if(ImGui::TreeNode("Visualization settings")) {
                        ImGui::Combo("Algorithm", &viz_item_current, viz_items, IM_ARRAYSIZE(viz_items));
                        adr.viz_method = viz_values[viz_item_current];

                        if ( adr.viz_method == "tsne" ) {
                            ImGui::Text("");

                            ImGui::Combo("Metric", &metric_item_current, metric_items, IM_ARRAYSIZE(metric_items));
                            adr.metric = metric_values[metric_item_current];
                            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_METRIC);

                            ImGui::InputInt("Perplexity", &adr.tsne_perplexity);
                            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_TSNE_PERPLEXITY);


                            ImGui::InputInt("Learning rate", &adr.tsne_learning_rate);
                            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_TSNE_LEARNING_RATE);

                            ImGui::InputInt("Iterations", &adr.tsne_iterations);
                            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_TSNE_ITERATIONS);
                        } else if ( adr.viz_method == "umap" ) {
                            ImGui::Text("");

                            ImGui::Combo("Metric", &metric_item_current, metric_items, IM_ARRAYSIZE(metric_items));
                            adr.metric = metric_values[metric_item_current];
                            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_METRIC);

                            ImGui::InputInt("N Neighbors", &adr.umap_n_neighbors);
                            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_UMAP_NEIGHBORS);

                            ImGui::InputFloat("Min distance", &adr.umap_min_dist, 0.05f, 0.1f, "%.2f");
                            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_UMAP_MIN_DISTANCE);
                        }

                        ImGui::Text("");


                        ImGui::TreePop();
                    }

                    if(ImGui::TreeNode("Preferences")){

                        ImGui::Checkbox("Save intermediate results", &adr.pca_results);
                        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_SAVE_PCA_RESULTS);
                        ImGui::Checkbox("Force full process", &adr.force_full_process);
                        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_FORCE_FULL_PROCESS);

                        ImGui::TreePop();
                    }


                    ImGui::TreePop();
                }

                ImGui::Text("");

                if(adr.dirPath.size()) {
                    if(ImGui::Button("Run")) {
                        haveToDrawDimReductScreen = false;
                        isDimReductScreenHovered = false;
                        isProcessingFiles = true;
                        isWelcomeScreenOpen = false;

                        adr.startThread();
                    }
                }else{
                    ImVec4 color = ImColor(0.3f, 0.3f, 0.3f, 0.5f);
                    ImVec4 textColor = ImColor(1.f, 1.0f, 1.f, 0.5f);
                    ImGui::PushStyleColor(ImGuiCol_Text, textColor);
                    ImGui::PushStyleColor(ImGuiCol_Button, color);
                    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, color);
                    ImGui::PushStyleColor(ImGuiCol_ButtonActive, color);
                    ImGui::Button("Run");
                    ImGui::PopStyleColor(4);
                }
                ImGui::End();
            }
            ImGui::PopStyleVar();

        }else {
            isDimReductScreenHovered = false;
        }
    }
}

void Gui::drawAudioSettingsScreen() {

    if(haveToDrawAudioSettingsScreen) {

        if(isAudioSettingsScreenOpen){

            ImGuiWindowFlags window_flags = 0;
            int w = 400;
            window_flags |= ImGuiWindowFlags_NoScrollbar;
            window_flags |= ImGuiWindowFlags_NoMove;
            window_flags |= ImGuiWindowFlags_NoResize;
            window_flags |= ImGuiWindowFlags_NoCollapse;
            window_flags |= ImGuiWindowFlags_NoNav;
            window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

            ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - w/2, 100));
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding,ImVec2(20,20));

            if (ImGui::Begin("Audio Settings" , &isAudioSettingsScreenOpen, window_flags)) {

                isAudioSettingsScreenHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem
                                                                      | ImGuiHoveredFlags_AllowWhenBlockedByPopup
                                                                      | ImGuiHoveredFlags_ChildWindows);

                // this can get better still
                vector <ofSoundDevice> devices = audioEngine->listDevices();

//                for (int i = 0; i < devices.size(); i++) {
//                    vector <string> name;
//                    name = ofSplitString(devices[i].name, ": ");
//                    if ( name.size() > 1 ) {
//                        devices[i].name = name[1];
//                    }
//                }

                if (ImGui::BeginCombo("Audio Out", devices[audioEngine->selectedAudioOutDeviceID].name.c_str())) {

                    for (int i = 0; i < devices.size(); i++) {
                        if (devices[i].outputChannels > 0) {
                            if (ImGui::Selectable( devices[i].name.c_str())) {
                                audioEngine->selectAudioOutDevice(devices[i].deviceID);
                            }
                            if (devices[i].deviceID == audioEngine->selectedAudioOutDeviceID) {
                                ImGui::SetItemDefaultFocus();
                            }
                        }
                    }
                    ImGui::EndCombo();
                }

                if (ImGui::BeginCombo( "Sample Rate", ofToString(audioEngine->selectedSampleRate).c_str() )) {

                    vector <ofSoundDevice> devices = audioEngine->listDevices();

                    for (int i = 0; i < devices[audioEngine->selectedAudioOutDeviceID].sampleRates.size(); i++) {
                        if (ImGui::Selectable( ofToString(devices[audioEngine->selectedAudioOutDeviceID].sampleRates[i]).c_str() )) {
                            audioEngine->selectSampleRate(devices[audioEngine->selectedAudioOutDeviceID].sampleRates[i]);
                        }
                    }
                    ImGui::EndCombo();
                }

                if (ImGui::BeginCombo( "Buffer Size", ofToString(audioEngine->selectedBufferSize).c_str() )) {

                    for (int i = 0; i < audioEngine->bufferSizes.size(); i++) {
                        if (ImGui::Selectable( ofToString(audioEngine->bufferSizes[i]).c_str() )) {
                            audioEngine->selectBufferSize(audioEngine->bufferSizes[i]);
                        }
                    }
                    ImGui::EndCombo();
                }

                ImGui::Text("");

                ImGui::InputInt("Max Voices",  &player->numVoices, NULL, NULL);
                if(ImGui::IsItemDeactivatedAfterEdit()) {
                    player->reset();
                }
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::AUDIO_SETTINGS_MAX_VOICES);

                ImGui::Text("");

//                ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0,0));
                ImGui::PushStyleVar(ImGuiStyleVar_ChildBorderSize, .0f);
                    ImGui::BeginChild("Child1", ImVec2(0,15), true);
                        ImGui::Columns(2, "columns1", false);
                        ImGui::Text("Stereo");
                        ImGui::NextColumn();
                        ImGui::Text("Mono");
                    ImGui::EndChild();
                ImGui::PopStyleVar();

                ImGui::Columns(1);

                ImGui::BeginChild("Child2", ImVec2(0,300), true, ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse);
                    ImGui::Columns(2, "columns2", false);

                    for (int i = 0; i < audioEngine->outputs.size(); i++) {
                        if (audioEngine->outputs[i].type == "stereo") {
                            ImGui::Selectable(audioEngine->outputs[i].label.c_str(), &audioEngine->outputs[i].enabled);
                        }
                    }

                    ImGui::NextColumn();

                    for (int i = 0; i < audioEngine->outputs.size(); i++) {
                        if (audioEngine->outputs[i].type == "mono") {

                            ImGui::Selectable(audioEngine->outputs[i].label.c_str(),
                                              &audioEngine->outputs[i].enabled,
                                              NULL,
                                              ImVec2(40.0f, .0f));

                            ImGui::SameLine(80);

                            ImGui::Selectable(audioEngine->outputs[i + 1].label.c_str(),
                                              &audioEngine->outputs[i + 1].enabled,
                                              NULL,
                                              ImVec2(40.0f, .0f));

                            i++; //skip next mono output
                        }
                    }

                ImGui::EndChild();

                ImGui::End();
            }
            ImGui::PopStyleVar();

        } else {
            isAudioSettingsScreenHovered = false;
        }
    }
}

void Gui::drawOscSettingsScreen(){
    if(haveToDrawOscSettingsScreen){
        if(isOscSettingsScreenOpen){

            ImGuiWindowFlags window_flags = 0;
            window_flags |= ImGuiWindowFlags_NoScrollbar;
            window_flags |= ImGuiWindowFlags_NoMove;
            window_flags |= ImGuiWindowFlags_NoResize;
            window_flags |= ImGuiWindowFlags_NoCollapse;
            window_flags |= ImGuiWindowFlags_NoNav;
            window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

            int w = 400;

            ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - w/2, ofGetHeight()/3));
            if(ImGui::Begin("OSC settings",
                            &isOscSettingsScreenOpen,
                            window_flags)){

                isOscSettingsScreenHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem
                                                                    | ImGuiHoveredFlags_AllowWhenBlockedByPopup
                                                                    | ImGuiHoveredFlags_ChildWindows);

                oscServer->drawGui();
                ImGui::End();
            }else{
                haveToDrawOscSettingsScreen = false;
                isOscSettingsScreenOpen = false;
            }
        }
        else {
            isOscSettingsScreenHovered = false;
        }
    }
}

void Gui::drawFPS()
{
    if ( haveToDrawFPS ) {
        ofSetColor(255);
        font.drawString(
            "FPS: " + ofToString(ofGetFrameRate(), 0),
            ofGetWidth() - 70,
            ofGetHeight() - 10);
    }
}

void Gui::drawMessage()
{
    if ( haveToDrawMessage ) {
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoResize;

        int w = 350;
        int h = 100;

        ImGui::SetNextWindowSize( ImVec2(w,h) );
        ImGui::SetNextWindowPos(ImVec2( (ofGetWidth()/2) - w/2, (ofGetHeight()/2) - h/2 ));

        ImGui::PushStyleVar(ImGuiStyleVar_Alpha, messageOpacity);

        if(ImGui::Begin( messageTitle.c_str() , &haveToDrawMessage, window_flags)) {

           isMessageWindowHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem
                                                           | ImGuiHoveredFlags_AllowWhenBlockedByPopup
                                                           | ImGuiHoveredFlags_ChildWindows);

           ImGui::PushTextWrapPos(w);
           ImGui::Text(messageDescription.c_str());
        }
        ImGui::End();

        ImGui::PopStyleVar();

        if ( messageFadeAway ) {
            messageOpacity -= 0.006f;
            if ( messageOpacity <= 0 ) {
                haveToDrawMessage = false;
                isMessageWindowHovered = false;
            }
        }

    } else {
        isMessageWindowHovered = false;
    }
}

void Gui::drawTutorial()
{
    ImGuiWindowFlags window_flags = 0;
    int w = 400;
    window_flags |= ImGuiWindowFlags_NoScrollbar;
    window_flags |= ImGuiWindowFlags_NoResize;
    window_flags |= ImGuiWindowFlags_NoCollapse;
    window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

//    ImGui::SetNextWindowPos(ImVec2(100, 100));

   if(isTutorialOpen){

       ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding,ImVec2(20,20));

       if (ImGui::Begin(tutorialCurrentPage->title.c_str() , &isTutorialOpen, window_flags)){
           isTutorialWindowHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem | ImGuiHoveredFlags_AllowWhenBlockedByPopup | ImGuiHoveredFlags_ChildWindows);

           ImGui::PushTextWrapPos(w);
           ImGui::TextWrapped(tutorialCurrentPage->content.c_str());

           string buttonTitle = "Next";
           if ( tutorial.isLastPage() ) {
               buttonTitle = "Finish";
           }

           if(ImGui::Button(buttonTitle.c_str())){
                tutorialCurrentPage = tutorial.getNextPage();
                if ( tutorialCurrentPage == nullptr ) {
                    isTutorialOpen = false;
                    isTutorialWindowHovered = false;
                }
           }

           ImGui::End();
       }

       ImGui::PopStyleVar();
   } else {
       isTutorialWindowHovered = false;
   }
}

void Gui::drawContextMenu() {

    if (haveToDrawSoundContextMenu) {

        string selectedSoundPath = selectedSound->getFileName();
        string buttonText;
        string command;

        #ifdef TARGET_WIN32
        buttonText = "Show in Explorer";
        command = "explorer /select,\"" + selectedSoundPath + "\"";
        #endif

        #ifdef TARGET_OSX
        buttonText = "Show in Finder";
        command = "open -R \"" + selectedSoundPath + "\"";
        #endif

        #ifdef TARGET_LINUX
        buttonText = "Show in File Manager";
        string hoveredSoundEnclosingDirectory = ofFilePath::getEnclosingDirectory(selectedSoundPath);
        string script {
            "file_manager=$(xdg-mime query default inode/directory | sed 's/.desktop//g'); "
            "if [ \"$file_manager\" = \"nautilus\" ] || [ \"$file_manager\" = \"dolphin\" ] || [ \"$file_manager\" = \"nemo\" ]; "
            "then command=\"$file_manager\"; path=\"%s\"; "
            "else command=\"xdg-open\"; path=\"%s\"; "
            "fi; "
            "$command \"$path\""
        };

        char buffer[ script.size() + selectedSoundPath.size() + hoveredSoundEnclosingDirectory.size() ];
        std::sprintf( buffer, script.c_str(), selectedSoundPath.c_str() ,
                     hoveredSoundEnclosingDirectory.c_str()  );
        command = buffer;
        #endif

        //Sound context menu
        ImGui::OpenPopup("soundMenu");
        if (ImGui::BeginPopup("soundMenu")) {
            isSoundContextMenuHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem
                                                               | ImGuiHoveredFlags_AllowWhenBlockedByPopup
                                                               | ImGuiHoveredFlags_ChildWindows);


            ImGui::TextDisabled("Sound Options");
            ImGui::Dummy(ImVec2(0, 3.0f));

            ImGui::SetNextWindowContentSize(ImVec2(100,0));
            if (ImGui::BeginMenu("Output Channels")) {

                for (int i = 0; i < audioEngine->outputs.size(); i++) {
                    if (audioEngine->outputs[i].enabled) {
                        if (ImGui::MenuItem( audioEngine->outputs[i].label.c_str(), NULL, selectedSound->selectedOutput == i) ) {

                            selectedSound->channel0 = audioEngine->outputs[i].channel0;
                            selectedSound->channel1 = audioEngine->outputs[i].channel1;

                            selectedSound->selectedOutput = i;

                            haveToDrawSoundContextMenu = false;
                            isSoundContextMenuHovered = false;

                        }
                    }
                }

                ImGui::EndMenu();
            }
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::CONTEXT_MENU_SOUND_OUTPUT_CHANNELS);


            ImGui::Separator();

            if (ImGui::MenuItem( "Mute", NULL, selectedSound->mute) ) {
                selectedSound->mute = !selectedSound->mute;
                ImGui::CloseCurrentPopup();
                haveToDrawSoundContextMenu = false;
                isSoundContextMenuHovered = false;
            }

            ImGui::Separator();

            if (ImGui::MenuItem( buttonText.c_str(), NULL, false) ) {
                ofSystem(command);
                ImGui::CloseCurrentPopup();
                haveToDrawSoundContextMenu = false;
                isSoundContextMenuHovered = false;
            }

//            ImGui::PopStyleVar();
            ImGui::EndPopup();
        }
    }

    if (haveToDrawClusterContextMenu) {

        vector <Sound *> cluster = sounds->getSoundsByCluster(selectedClusterID);

        ImGui::OpenPopup("clusterMenu");
        if (ImGui::BeginPopup("clusterMenu")) {
            isClusterContextMenuHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem
                                                                 | ImGuiHoveredFlags_AllowWhenBlockedByPopup
                                                                 | ImGuiHoveredFlags_ChildWindows);

            ImGui::TextDisabled("Cluster Options");
            ImGui::Dummy(ImVec2(0, 3.0f));

            ImGui::AlignTextToFramePadding();
            ImGui::Text("Name:");

            ImGui::SameLine();

            if (!isClusterContextMenuFirstOpen) {
                for ( int i = 0 ; i < CLUSTERNAME_MAX_LENGTH ; i++ ) {
                    sounds->clusterNameBeingEdited[i] = sounds->clusterNames[selectedClusterID][i];
                }

                isClusterContextMenuFirstOpen = true;
            }

            ImGui::PushItemWidth(150);
            if (ImGui::InputText("##ClusterName", sounds->clusterNameBeingEdited, 64, ImGuiInputTextFlags_EnterReturnsTrue |
                                                             ImGuiInputTextFlags_AutoSelectAll )) {

                sounds->nameCluster(selectedClusterID);
                haveToDrawClusterContextMenu = false;
                isClusterContextMenuHovered = false;

            }
            else if (ImGui::IsItemEdited()) {
                sounds->nameCluster(selectedClusterID);
            }
            isClusterContextMenuFirstOpen = ImGui::IsItemActive();
//            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::CLUSTERS_SET_NAME);
            ImGui::Separator();

            ImGui::SetNextWindowContentSize(ImVec2(100,0));
            if (ImGui::BeginMenu("Output Channels")) {

                for (int i = 0; i < audioEngine->outputs.size(); i++) {
                    if (audioEngine->outputs[i].enabled) {

                        // check if all sounds in cluster share the same output
                        int output = cluster[0]->selectedOutput;
                        bool sameOutput = true;
                        for (int i = 0; i < cluster.size(); i++) {
                            if (cluster[i]->selectedOutput != output) {
                                sameOutput = false;
                            }
                        }

                        if (ImGui::MenuItem( audioEngine->outputs[i].label.c_str(), NULL, (cluster[0]->selectedOutput == i) && sameOutput) ) {

                            for (int j = 0; j < cluster.size(); j++) {
                                cluster[j]->channel0 = audioEngine->outputs[i].channel0;
                                cluster[j]->channel1 = audioEngine->outputs[i].channel1;

                                cluster[j]->selectedOutput = i;

                            }

                            haveToDrawClusterContextMenu = false;
                            isClusterContextMenuHovered = false;
                        }
                    }
                }

                ImGui::EndMenu();
            }
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::CONTEXT_MENU_CLUSTER_OUTPUT_CHANNELS);

            ImGui::Separator();

            // check if all sounds in cluster are muted
            bool allMuted = true;
            for (int i = 0; i < cluster.size(); i++) {
                if (!cluster[i]->mute) {
                    allMuted = false;
                }
            }

            if (ImGui::MenuItem("Mute", NULL, allMuted) ) {

                for (int i = 0; i < cluster.size(); i++) {
                    cluster[i]->mute = !allMuted;
                }

                ImGui::CloseCurrentPopup();
                haveToDrawClusterContextMenu = false;
                isClusterContextMenuHovered = false;
            }

            ImGui::EndPopup();
        }
        else {
            haveToDrawClusterContextMenu = false;
            isClusterContextMenuHovered = false;
        }
    }
}

void Gui::showMessage(string description, string title, bool fadeAway)
{
    haveToDrawMessage = true;
    messageTitle = title;
    messageDescription = description;
    messageOpacity = 1.0f;
    messageFadeAway = fadeAway;
}

void Gui::hideMessage()
{
    haveToDrawMessage = false;
}

void Gui::showWelcomeScreen()
{
    isWelcomeScreenOpen = true;
}

void Gui::startTutorial()
{
    tutorialCurrentPage = tutorial.start();
    isTutorialOpen = true;
}

void Gui::keyPressed(ofKeyEventArgs & e) {

    if (haveToDrawClusterContextMenu) {
        return;
    }

    Sounds::getInstance()->keyPressed(e);

    if(e.key == 'g') {
        drawGui = !drawGui;
        if(!drawGui){
            forceGuiHoveredState(false);
        }
    }


    bool controlOrCommand = (e.hasModifier(OF_KEY_CONTROL)||e.hasModifier(OF_KEY_COMMAND));

    #ifndef TARGET_OS_MAC
    if ( e.key == 'S' && controlOrCommand ) //with shift
    #else
    if ( e.key == 's' && controlOrCommand && e.hasModifier(OF_KEY_SHIFT) )
    #endif
        sessionManager->saveAsNewSession();
    else if ( e.key == 's' && controlOrCommand )
        if ( !sessionManager->isDefaultSession ) {
            sessionManager->saveSession();
        } else {
            showMessage("Cannot save default session, use \"Save as\" instead");
        }
    else if ( e.key == 'n' && controlOrCommand )
        newSession();
    else if ( e.key == 'o' && controlOrCommand ) {
        SessionManager::datasetLoadOptions opts;
        opts.method = "GUI";
        sessionManager->loadSession(opts);
    }
}

void Gui::mousePressed(ofVec2f p, int button) {
    if (!isMouseHoveringGUI()) {
        if (button != 2) {
            haveToDrawSoundContextMenu = false;
            haveToDrawClusterContextMenu = false;
            isClusterContextMenuFirstOpen = false;
        }
    }
}

void Gui::mouseReleased(ofVec2f p, int button)
{
    if (!isMouseHoveringGUI()) {

        if (button == 2 && !sounds->soundIsBeingMoved) {

            Sound * hoveredSound = sounds->getHoveredSound();

            if ( hoveredSound != NULL ) {
                haveToDrawSoundContextMenu = true;
                selectedSound = hoveredSound;
            }
            else haveToDrawSoundContextMenu = false;


            if (hoveredSound == NULL && sounds->hoveredClusterID >= 0) {
                haveToDrawClusterContextMenu = true;
                if (!isClusterContextMenuFirstOpen) {
                    selectedClusterID = sounds->hoveredClusterID;
                }
            }
            else haveToDrawClusterContextMenu = false;

        }
        else {
            haveToDrawSoundContextMenu = false;
            haveToDrawClusterContextMenu = false;
            isClusterContextMenuFirstOpen = false;
        }
    }
    else if ( !isClusterContextMenuHovered && !isSoundContextMenuHovered ) {
        haveToDrawSoundContextMenu = false;
        haveToDrawClusterContextMenu = false;
        isClusterContextMenuFirstOpen = false;
    }
}

bool Gui::isMouseHoveringGUI() {

    return ( isToolsWindowHovered
            || isMainMenuHovered
            || isAboutScreenHovered
            || isWelcomeScreenHovered
            || isDimReductScreenHovered
            || isProcessingFilesWindowHovered
            || isMessageWindowHovered
            || isTutorialWindowHovered
            || isAudioSettingsScreenHovered
            || isOscSettingsScreenHovered
            || isSoundContextMenuHovered
            || isClusterContextMenuHovered
            || modes->isMouseHoveringGui()
            || sounds->isMouseHoveringGUI() );

}

void Gui::forceGuiHoveredState(bool state){

    isToolsWindowHovered = state;
    isMainMenuHovered = state;
    isAboutScreenHovered = state;
    isWelcomeScreenHovered = state;
    isDimReductScreenHovered = state;
    isProcessingFilesWindowHovered = state;
    isMessageWindowHovered = state;
    isTutorialWindowHovered = state;


}
