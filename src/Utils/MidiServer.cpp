#include "MidiServer.h"

bool MidiServer::midiLearn = false;

MidiServer::MidiServer() {
	defaultlastControlMoved.label = "";
	defaultlastControlMoved.cc = -1;
	defaultlastControlMoved.float_ptr = NULL;
	defaultlastControlMoved.int_ptr = NULL;
	defaultlastControlMoved.min = -1.0f;
	defaultlastControlMoved.max = -1.0f;

	//si hay un error aca, dale git pull a addons/ofxMidi
    midiDevices = midiIn.getInPortList();
    
    lastControlMoved = defaultlastControlMoved;

}

void MidiServer::init(Modes *_modes){
    modes = _modes;

    midiIn.setVerbose(true);
    midiIn.addListener(this);
}

void MidiServer::drawGui(){
    ImGui::Text("MIDI Devices");
    if(ImGui::BeginCombo("##MIDI Devices", currentMidiDevice.c_str())){

       checkForNewMidiDevices();

       for(int i = 0; i < midiDevices.size(); i++){

           bool isSelected = (currentMidiDevice == midiDevices[i]);

           if(ImGui::Selectable(midiDevices[i].c_str() )){
               selectMidiDevice(i);
           }

           if(isSelected){
               ImGui::SetItemDefaultFocus();
           }

       }
            ImGui::EndCombo();
    }

    ImGui::Separator();

    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::MIDI_DEVICES);
}

void MidiServer::checkForNewMidiDevices(){
    midiDevices = midiIn.getInPortList();
}

void MidiServer::selectMidiDevice(int idx) {
    currentMidiDevice = midiDevices[idx];
    //primero cerrá el puerto del device anterior
    midiIn.closePort();
    //abrí el nuevo puerto
    midiIn.openPort(currentMidiDevice);
    midiIn.ignoreTypes(true,false,true); //no ignores los de clock
}


void MidiServer::newMidiMessage(ofxMidiMessage& msg) {
    Utils::midiMsg m;
    
    //    ofLog() << "Message" << ofxMidiMessage::getStatusString(msg.status);
    
    // Los status puede ser:
    // Time Clock / Time Code / Note On / Note Off / Control Change
    //TODO: Guarda que hay Constantes !! No usar String
    
    m.status = ofxMidiMessage::getStatusString(msg.status);
    m.pitch =  msg.pitch;
    m.cc = msg.control;
    m.ccVal = msg.value;
    m.beats = -1;
    m.bpm = -1;

    if(clock.update(msg.bytes)) {
        
        // a MIDI beat is a 16th note, so do a little math to convert to a time signature:
        // 4/4 -> 4 notes per bar & quarter note = 1 beat, add 1 to count from 1 instead of 0
        unsigned int beats = clock.getBeats();

        int normalizedBeat = ( beats % 16 );
        if ( normalizedBeat != currentBeat ) {
            currentBeat = normalizedBeat;
            m.beats = currentBeat;
        } else {
            return; //no envies el mensaje si es repetido
        }
        
        //        int quarters = beats / 4; // convert total # beats to # quarters
        //        int bars = (quarters / 4) + 1; // compute # of bars
        //        int beatsInBar = (quarters % 4) + 1; // compute remainder as # notes within the current bar
        //        m.seconds = clock.getSeconds();
    }

    //CONTROL CHANGE
    if(msg.status == MIDI_CONTROL_CHANGE || msg.status == MIDI_PITCH_BEND) {

        //Medio hacky pero bueeeh
        if(msg.status == MIDI_PITCH_BEND){
            m.cc = 128;
        }

        if(midiLearn){
            if(lastControlMoved.label != ""){
                lastControlMoved.cc = m.cc;
                ccMappings.push_back(lastControlMoved);
                midiLearn = !midiLearn;
            }
        } else {
            for(unsigned int i = 0; i < ccMappings.size(); i++){
                if(ccMappings[i].cc == m.cc) {
                    if(ccMappings[i].int_ptr != NULL){

                        if(msg.status == MIDI_PITCH_BEND){

                            *ccMappings[i].int_ptr = ofMap(m.ccVal
                                                       , 0
                                                       , 16383
                                                       ,  ccMappings[i].min
                                                       ,  ccMappings[i].max);

                        }else{

                            *ccMappings[i].int_ptr = ofMap(m.ccVal
                                                       , 0
                                                       , 127
                                                       , ccMappings[i].min
                                                       , ccMappings[i].max);

                        }
                    }

                    if(ccMappings[i].float_ptr != NULL){

                        if(msg.status == MIDI_PITCH_BEND){
                            *ccMappings[i].float_ptr = ofMap(m.ccVal
                                                       , 0
                                                       , 16383
                                                       ,  ccMappings[i].min
                                                       ,  ccMappings[i].max);

                        }else{

                            *ccMappings[i].float_ptr = ofMap(m.ccVal
                                                       , 0
                                                       , 127
                                                       , ccMappings[i].min
                                                       , ccMappings[i].max);

                        }

                        *ccMappings[i].float_ptr = trunc_decs(*ccMappings[i].float_ptr, 2);

                    }
                }
            }
        }
    } else {
        modes->midiMessage(m);
    }
}

bool MidiServer::SliderFloat(string label, float *ptr, float min, float max){

    bool sliderInteraction = ImGui::SliderFloat(label.c_str(), ptr, min, max);

    for(unsigned int i = 0; i < ccMappings.size(); i ++){

        if(label == ccMappings[i].label){
            ccMappings[i].float_ptr = ptr;
        }
    }

    if(sliderInteraction){

        if(midiLearn){

            //lastControlMoved = { .label = label, .cc = -1, .float_ptr = ptr, .int_ptr = NULL, .min = min, .max = max};
			lastControlMoved.label = label;
			lastControlMoved.cc = -1;
			lastControlMoved.float_ptr = ptr;
			lastControlMoved.int_ptr = NULL;
			lastControlMoved.min = min;
			lastControlMoved.max = max;
			
        }else{

            lastControlMoved = defaultlastControlMoved;

        }

    }

    return sliderInteraction;
}

bool MidiServer::VSliderFloat(string label, const ImVec2 &size, float *ptr, float min, float max, string format){
   bool sliderState = ImGui::VSliderFloat(label.c_str(), size, ptr, min, max, format.c_str());

    for(unsigned int i = 0; i < ccMappings.size(); i ++){
        if(label == ccMappings[i].label){
            ccMappings[i].float_ptr = ptr;
        }
    }

    if(sliderState){
        if(midiLearn){
            //lastControlMoved = { .label = label, .cc = -1, .float_ptr = ptr, .int_ptr = NULL, .min = min, .max = max};
			lastControlMoved.label = label;
			lastControlMoved.cc = -1;
			lastControlMoved.float_ptr = ptr;
			lastControlMoved.int_ptr = NULL;
			lastControlMoved.min = min;
			lastControlMoved.max = max;
        }else{
            lastControlMoved = defaultlastControlMoved;
        }
    }

   return sliderState;
}

bool MidiServer::SliderInt(string label, int *ptr, int min, int max){
    bool sliderState = ImGui::SliderInt(label.c_str(), ptr, min, max);

    for(unsigned int i = 0; i < ccMappings.size(); i ++){
        if(label == ccMappings[i].label){
            ccMappings[i].int_ptr = ptr;
        }
    }

    if(sliderState){
        if(midiLearn){
            //lastControlMoved = { .label = label, .cc = -1, .float_ptr = NULL, .int_ptr = ptr, .min = float(min), .max = float(max)};
			lastControlMoved.label = label;
			lastControlMoved.cc = -1;
			lastControlMoved.float_ptr = NULL;
			lastControlMoved.int_ptr = ptr;
			lastControlMoved.min = min;
			lastControlMoved.max = max;
        }else{
            lastControlMoved = defaultlastControlMoved;
        }
    }

    return sliderState;
}


Json::Value MidiServer::save(){
    Json::Value root = Json::Value(Json::arrayValue);

    for(unsigned int i = 0; i < ccMappings.size(); i++){

       Json::Value val = Json::Value(Json::objectValue);

       val["label"] = ccMappings[i].label;
       val["cc"] = ccMappings[i].cc;
       val["min"] = ccMappings[i].min;
       val["max"] = ccMappings[i].max;

       root[i] = val;
    }

   return root;
}

void MidiServer::load(Json::Value jsonData){
    if(jsonData != Json::nullValue){

        for(unsigned int i = 0; i < jsonData.size(); i++){
           Json::Value c = jsonData[i];
           midiCCMapping mapping;

           mapping.label = ofToString(c["label"]);
           mapping.cc = c["cc"].asInt();
           mapping.float_ptr = NULL;
           mapping.int_ptr = NULL;
           mapping.min = c["min"].asFloat();
           mapping.max = c["max"].asFloat();

           ofStringReplace(mapping.label, "\"", "");
           ofStringReplace(mapping.label, "\n", "");
           //ofStringReplace(mapping.label, " ", "");

           ccMappings.push_back(mapping);
        }

    }

}

void MidiServer::reset(){
  ccMappings.clear();
}

string MidiServer::getCurrentMidiDevice(){
  return currentMidiDevice;
}

void MidiServer::setCurrentMidiDevice(string device){

  int port = -1;
	for(unsigned int i = 0; i < midiIn.getNumInPorts(); ++i) {
		string name = midiIn.getInPortName(i);
		if(name == device) {
			port = i;
			break;
		}
	}

	if(port == -1) {
	  ofLogNotice("WARN", "The last midi device used is not connected");	
	}else{
    midiIn.closePort();
    midiIn.openPort(device);
    midiIn.ignoreTypes(true,false,true); //no ignores los de clock
    currentMidiDevice = device;
  }
	
}

template<typename F>
F MidiServer::trunc_decs(const F &f, int decs)
{
    int i1 = floor(f);
    F rmnd = f - i1;
    int i2 = static_cast<int> (rmnd * pow(10, decs));
    F f1 = i2 / pow(10, decs);

    return i1 + f1;
}
