#include "ofMain.h"
#include "ofApp.h"
#include "ofAppGLFWWindow.h"

//========================================================================
int main( int argc, char *argv[] ) {
    ofSetupOpenGL(1280,720, OF_WINDOW);			// <-------- setup the GL context
    ofApp *app = new ofApp();

    app->arguments = vector<string>(argv, argv + argc);

    #ifdef TARGET_LINUX
    //Need to make setWindowIcon public in libs/openFrameworks/app/ofAppGLFWWindow.h to work
    ofAppGLFWWindow* win;
    win = dynamic_cast<ofAppGLFWWindow *> (ofGetWindowPtr());
    win->setWindowIcon("assets/icon_256.png");
    #endif

    ofRunApp(app);
}

